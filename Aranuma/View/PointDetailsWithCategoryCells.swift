//
//  PointDetailsWithCategoryCells.swift
//  Aranuma
//
//  Created by topcoder on 2/23/19.
//  Copyright © 2019 aranuma. All rights reserved.
//

import Foundation

class PointDetailsWithCategory: UITableViewCell {
    @IBOutlet weak var lblPointFullName: UILabel!
    @IBOutlet weak var lblPointValue: UILabel!
    @IBOutlet weak var lblPointTimestamp: UILabel!
    @IBOutlet weak var lblPointQuality: UILabel!
    @IBOutlet weak var lblPointAliesName: UILabel!
    
    func ConfigCells(point: CategoryDetailPointModel) {
        
    }
}
