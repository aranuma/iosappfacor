//
//  PointDetailsCellTableViewCell.swift
//  Aranuma
//
//  Created by topcoder on 12/22/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import UIKit

class PointDetailsCell: UITableViewCell {

    @IBOutlet weak var lblPointFullName: UILabel!
    @IBOutlet weak var lblPointValue: UILabel!
    @IBOutlet weak var lblPointTimestamp: UILabel!
    @IBOutlet weak var lblPointQuality: UILabel!
    @IBOutlet weak var lblPointAliesName: UILabel!
    
    func ConfigCells(point: CategoryDetailPointModel) {
        let slicedPoint = point.pointName.components(separatedBy: ".")
        
        if point.pointAliesName.isEmpty {
            lblPointAliesName.text = slicedPoint[slicedPoint.count - 1]
        } else {
            
            let slicedAliesName = point.pointAliesName.components(separatedBy: "-")
            
            if !slicedAliesName[slicedAliesName.count - 1].contains("Empty") {
                self.lblPointAliesName.text = slicedAliesName[slicedAliesName.count - 1]
            } else {
                self.lblPointAliesName.text = slicedPoint[slicedPoint.count - 1]
            }
        }
        
        lblPointFullName.text = "    Name: " + slicedPoint[slicedPoint.count - 1]
        lblPointValue.text = "    Value: " +  point.pointValue
        
        if point.pointQuality == "192" {
            lblPointQuality.text = "  Good"
        } else {
            lblPointQuality.text = "  Bad"
        }
        
        lblPointTimestamp.text = "    Time: " + point.pointTimeStamp
    }
}

extension StringProtocol where Index == String.Index {
    var byWords: [SubSequence] {
        var byWords: [SubSequence] = []
        enumerateSubstrings(in: startIndex..., options: .byWords) { _, range, _, _ in
            byWords.append(self[range])
        }
        return byWords
    }
}
