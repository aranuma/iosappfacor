//
//  ButtonWithGrayGradient.swift
//  Aranuma
//
//  Created by topcoder on 2/19/19.
//  Copyright © 2019 aranuma. All rights reserved.
//

import UIKit


@IBDesignable
class ButtonBorder: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.borderWidth = 1.0
        
        layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        layer.masksToBounds = true
        
    }
}
