//
//  CustomViewButton_Login.swift
//  Aranuma
//
//  Created by topcoder on 10/29/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedButton: UIButton {
    
    override func prepareForInterfaceBuilder() {
        CustomView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        CustomView()
    }
    
    private func CustomView() {
        backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.layer.cornerRadius = 0.5 * bounds.size.width
        clipsToBounds = true
    }
}
