//
//  CustomTextField_Login.swift
//  CalculatorPlus
//
//  Created by topcoder on 11/27/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import UIKit

@IBDesignable
class CustomTextField_Login: UITextField {
    override func prepareForInterfaceBuilder() {
        CustomView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        CustomView()
    }
    
    private func CustomView() {
        backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.1966235017)
        layer.borderWidth = 1
        self.layer.cornerRadius = 12
        self.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        self.textAlignment = .center
        if placeholder == nil {
            placeholder = " "
        }
        if let text = placeholder {
            let place = NSAttributedString(string: text, attributes: [.foregroundColor: #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)])
            attributedPlaceholder = place
            textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        
        self.enablesReturnKeyAutomatically = true
    }
    
}
