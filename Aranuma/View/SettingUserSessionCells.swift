//
//  SettingUserSessionCells.swift
//  Aranuma
//
//  Created by topcoder on 1/2/19.
//  Copyright © 2019 aranuma. All rights reserved.
//

import UIKit

class SettingUserSessionCells: UICollectionViewCell {

    @IBOutlet weak var lblShowSession: UILabel!
    
    func Config(session: SessionModel) {
        self.lblShowSession.text = session.session
    }
}
