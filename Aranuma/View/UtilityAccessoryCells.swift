//
//  UtilityAccessoryCells.swift
//  Aranuma
//
//  Created by topcoder on 11/3/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import UIKit

class UtilityAccessoryCells: UITableViewCell {

    @IBOutlet weak var lblHeader:  UILabel!
    @IBOutlet weak var lblPointValueName:  UILabel!
    @IBOutlet weak var lblPointTimeStamp:  UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(point: PointsModel) {
        if point.pointName == "" {
            lblHeader.text = "LEVEL"
            lblPointValueName.text = ""
            lblPointTimeStamp.text = ""
            lblPointValueName.isHidden = true
            lblPointTimeStamp.isHidden = true
        } else {
            lblHeader.text = ""
            let name = point.pointName ?? ""
            let value = point.pointValue ?? ""
            let timestamp = point.pointTimeStamp ?? ""
            //let quality = point.pointQuality ?? ""
            lblPointValueName.text = name + ": " + value
            lblPointTimeStamp.text = timestamp
        }
    }
}
