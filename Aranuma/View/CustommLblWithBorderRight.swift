//
//  CustommLblWithBorderRight.swift
//  Aranuma
//
//  Created by topcoder on 2/18/19.
//  Copyright © 2019 aranuma. All rights reserved.
//

import UIKit

@IBDesignable
class CustomLblWithBorderRight: UILabel {
    override func prepareForInterfaceBuilder() {
        CustomView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        CustomView()
    }
    
    private func CustomView() {
        
        layer.borderWidth = 1.0
        
        clipsToBounds = true
        layer.cornerRadius = 10
        layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        
        layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        layer.masksToBounds = true
    }
}
