//
//  SettingUserLogCellsCollectionViewCell.swift
//  Aranuma
//
//  Created by topcoder on 1/2/19.
//  Copyright © 2019 aranuma. All rights reserved.
//

import UIKit

class SettingUserLogCellsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblShowEvent: UILabel!
    @IBOutlet weak var lblShowTimeOfEvent: UILabel!
    
    public func ConfigCell(logs: UserLogModel) {
        self.lblShowEvent.text = logs._event
        self.lblShowTimeOfEvent.text = logs.time
    }
    
}
