//
//  SessionModel.swift
//  Aranuma
//
//  Created by topcoder on 1/2/19.
//  Copyright © 2019 aranuma. All rights reserved.
//

import Foundation

class SessionModel {
    private(set) public var session: String!
    
    init (session: String) {
        self.session = session
    }
}
