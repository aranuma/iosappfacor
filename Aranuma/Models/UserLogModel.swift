//
//  UserLogModel.swift
//  Aranuma
//
//  Created by topcoder on 1/2/19.
//  Copyright © 2019 aranuma. All rights reserved.
//

import Foundation

class UserLogModel {
    private(set) public var _event: String!
    private(set) public var time: String!
    
    init (_event: String, time: String) {
        self._event = _event
        self.time = time
    }
}
