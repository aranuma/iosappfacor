//
//  ChartCategoryModel.swift
//  Aranuma
//
//  Created by topcoder on 2/12/19.
//  Copyright © 2019 aranuma. All rights reserved.
//

import Foundation

class ChartCategoryModel {
    private(set) public var deviceName: String!
    private(set) public var deviceCategory: String!
    
    init (deviceName: String, deviceCategory: String) {
        self.deviceName = deviceName
        self.deviceCategory = deviceCategory
    }
}
