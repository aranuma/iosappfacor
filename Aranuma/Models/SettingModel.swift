//
//  SettingModel.swift
//  Aranuma
//
//  Created by topcoder on 1/1/19.
//  Copyright © 2019 aranuma. All rights reserved.
//

import Foundation

class SettingModel {
    private(set) public var title: String!
    private(set) public var description: String!
    
    init(title: String, description: String) {
        self.title = title
        self.description = description
    }
}
