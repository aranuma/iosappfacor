//
//  CategoryDetailPointModel.swift
//  Aranuma
//
//  Created by topcoder on 2/18/19.
//  Copyright © 2019 aranuma. All rights reserved.
//

import Foundation

import Foundation

struct CategoryDetailPointModel: Decodable {
    
    private(set) public var pointName: String!
    private(set) public var pointTimeStamp: String!
    private(set) public var pointQuality: String!
    private(set) public var pointValue: String!
    private(set) public var pointAliesName: String!
    
    init (pointName: String, pointValue: String, pointQuality: String, pointTimeStamp: String, pointAliesName: String) {
        self.pointName = pointName
        self.pointValue = pointValue
        self.pointQuality = pointQuality
        self.pointTimeStamp = pointTimeStamp
        self.pointAliesName = pointAliesName
    }
}
