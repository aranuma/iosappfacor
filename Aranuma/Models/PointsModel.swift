//
//  PointsModel.swift
//  Aranuma
//
//  Created by topcoder on 10/29/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import Foundation

struct PointsModel: Decodable {
    private(set) public var pointName: String!
    private(set) public var pointTimeStamp: String!
    private(set) public var pointQuality: String!
    private(set) public var pointValue: String!
    
    init (pointName: String, pointValue: String, pointQuality: String, pointTimeStamp: String) {
        self.pointName = pointName
        self.pointValue = pointValue
        self.pointQuality = pointQuality
        self.pointTimeStamp = pointTimeStamp
    }
}
