//
//  GetPointsService.swift
//  Aranuma
//
//  Created by topcoder on 11/4/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class GetPointService {
    
    static let instance = GetPointService()
    
    var pointlists = [PointsModel]()
    
    func GetImageFromCamera(completion: @escaping CompletionHandler) {
        
        let queryString: Parameters = [
            "pointName": [
                "FAC_JAL_001.Camera:Image"
            ]
        ]
        
        Alamofire.request(URL_GET_SelectedPoint, method: .post,
                          parameters: queryString, encoding: URLEncoding(destination: .queryString),
          headers: HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                
                do {
                    if let json = try JSON(data: data).array {
                        for item in json {
                            let name = item["name"].stringValue
                            let quality = item["quality"].stringValue
                            let timestamp = item["timestamp"].stringValue
                            let value = item["value"].stringValue
                            let pointlist = PointsModel(pointName: name, pointValue: value,
                                                        pointQuality: quality, pointTimeStamp: timestamp)
                            self.pointlists.append(pointlist)
                        }
                        completion(true)
                    }
                } catch {
                    completion(false)
                }
            } else {
                completion(false)
            }
        }
    }
}
