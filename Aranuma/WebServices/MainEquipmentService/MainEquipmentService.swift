//
//  MainEquipmentService.swift
//  Aranuma
//
//  Created by topcoder on 11/20/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class MainEquipmentService {
    
    static let instance = MainEquipmentService()
    
    var pointList = [PointsModel]()
    
    func GetPoints(completion: @escaping CompletionHandler) {
        let queryString: Parameters = [
            "pointName": [
                //--------------- HPGRs points
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.120MI01-ME01-SP",// >> Roll speed 1 float
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.120MI01-ME02-SP", // >> Roll speed 2 fix
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.120MI01ME01_Power", // => Power HPGR #1 (KW / H)
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.120MI01ME02_Power", // => Power HPGR #2 (KW / H)
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.120MI01_GT01", // Drive Side Gap
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.120MI01_GT02",// Non Drive Side Gap
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.120MI01-HU01_PT01", //DRIVE SIDE HYDRAULIC
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.120MI01-HU01_PT02", // Non Drive Side Hydraulic Roll
                
                //--------------- BALLMILLs points
                "FAC_JAL_001:SYM_.BALLMILL.CPU 414-3 DP.120MI02-ME01_P", // => Power (KW / H)
                "FAC_JAL_001:SYM_.BALLMILL.CPU 414-3 DP.120MI02-GB01_VT01", // => Vibration 1
                "FAC_JAL_001:SYM_.BALLMILL.CPU 414-3 DP.120MI02_FIT01", //
                //--------------- Hidrocyclone points
                "FAC_JAL_001:SYM_.BALLMILL.CPU 414-3 DP.120HC01_PT01", // cyclone pressure (bar)
                "FAC_JAL_001:SYM_.BALLMILL.CPU 414-3 DP.120HC01_DT01", // DT
                //--------------- thickeners points
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.150TH02-CE01_RL", // Rack level from bottom
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.150TH02-CE01_BL", // Bed level
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.150TH02-CE01_LD", // Rack Pressure
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.150TH02-CE01_BM", // BedMass Pressure
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.150TH02-CE01_SM", // BedMass Solid Matter
                
                //-------------- Motors
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.120MI01ME01-Run", // HPGR ==> Motor 1
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.120MI01ME02-Run", // HPGR ==> Motor 2
                
                "FAC_JAL_001:SYM_.BALLMILL.CPU 414-3 DP.120MI02-ME01_Run", // BallMill ==> Motor 1
                
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.150TH02-CE01_ON", //Thickener panel Running (ON/OFF)
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.150FL02-CE01_ON" //  Flocculant panel running (ON/OFF)
            ]
        ]
        
        Alamofire.request(URL_GET_SelectedPoint, method: .post, parameters: queryString, encoding: URLEncoding(destination: .queryString), headers: HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                
                do {
                    if let json = try JSON(data: data).array {
                        for item in json {
                            let name = item["name"].stringValue
                            let quality = item["quality"].stringValue
                            let timestamp = item["timestamp"].stringValue
                            let value = item["value"].stringValue
                            let pointlist = PointsModel(pointName: name, pointValue: value,
                                                        pointQuality: quality, pointTimeStamp: timestamp)
                            self.pointList.append(pointlist)
                        }
                        completion(true)
                    }
                } catch {
                    completion(false)
                }
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
}
