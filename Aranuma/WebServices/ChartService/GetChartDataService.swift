//
//  GetCharDataService.swift
//  Aranuma
//
//  Created by topcoder on 11/27/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class GetChartDataService {
    
    static let instance = GetChartDataService()
    
    var pointTimeStamp = [String]()
    var pointValue = [Float32]()
    
    func GetPoint(token: String, requestPoint: String, time: String, completion: @escaping CompletionHandler) {
        
        pointTimeStamp.removeAll()
        pointValue.removeAll()
        
        let queryString: Parameters = [
            "token": token,
            "tblName": requestPoint,
            "startTime": time
        ]
        
        Alamofire.request(URL_GET_ChartService, method: .get, parameters: queryString, headers: HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                
                do {
                    if let json = try JSON(data: data).array {
                        for item in json {
                            self.pointValue.append((item["value"].stringValue as NSString).floatValue)
                            self.pointTimeStamp.append(item["time"].stringValue)
                        }
                        completion(true)
                    }
                } catch {
                    completion(false)
                    debugPrint(response.result.error as Any)
                }
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
}
