//
//  InitChartPointsService.swift
//  Aranuma
//
//  Created by topcoder on 1/1/19.
//  Copyright © 2019 aranuma. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class InitChartPointsService {
    
    static let instance = InitChartPointsService()
    
    var ChartCategories = [ChartCategoryModel]()
    
    var ChartDevicePoints = [String]()
    
    var SavedDevicesName = [String]()
    
    func GetDeviceCategories(token: String, completion: @escaping CompletionHandler) {
        
        ChartCategories.removeAll()
        
        let queryString: Parameters = [
            "token": token
        ]
        
        Alamofire.request(URL_GET_GetAllDevicePoints_Charts, method: .get, parameters: queryString, headers: HEADER).responseJSON {
            (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                
                do {
                    if let json = try JSON(data: data).array {
                        for item in json {
                            let deviceName = item["deviceName"].stringValue
                            let deviceCategory = item["deviceCategory"].stringValue
                            let chartCategoryModel = ChartCategoryModel(deviceName: deviceName, deviceCategory: deviceCategory)
                            
                            self.ChartCategories.append(chartCategoryModel)
                        }
                        completion(true)
                    }
                } catch {
                    completion(false)
                    debugPrint(response.result.error as Any)
                }
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
        
    }
    
    func GetPointOfDevice(token: String, category: String, completion: @escaping CompletionHandler) {
        ChartDevicePoints.removeAll()
        
        let queryString: Parameters = [
            "token": token,
            "category": category
        ]
        
        Alamofire.request(URL_GET_GetAllPointsOfSelectedDevice_Charts, method: .get, parameters: queryString, headers: HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                
                do {
                    if let json = try JSON(data: data).array {
                        for item in json {
                            self.ChartDevicePoints.append(item.stringValue)
                        }
                        completion(true)
                    }
                } catch {
                    completion(false)
                    debugPrint(response.result.error as Any)
                }
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
}
