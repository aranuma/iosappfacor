//
//  PlantConditionService.swift
//  Aranuma
//
//  Created by topcoder on 11/19/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class PlantConditionService {
    
    static let instance = PlantConditionService()
    
    var pointsList = [PointsModel]()
    
    func GetPoints(completion: @escaping CompletionHandler) {
        
        let queryString: Parameters = [
            "pointName": [
                //..........Input (f)
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.110WE01-CE01_WI", // input (F) => Rate T/H
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.110WE01-CE01_Total", // input (F) => Cum.F(Cumulative)
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.110WE01-CE01_Shift", // input (F) => Shift
                //..........Output (C)
                "FAC_JAL_001:SYM_.BALLMILL.CPU 414-3 DP.140WE01-CE01_WI", // output (C) => Rate T/H
                "FAC_JAL_001:SYM_.BALLMILL.CPU 414-3 DP.140WE01-CE01_Total", // output (C) => Rate T/H
                "FAC_JAL_001:SYM_.BALLMILL.CPU 414-3 DP.140WE01-CE01_Shift",
                
                "FAC_JAL_001:SYM_.BALLMILL.CPU 414-3 DP.150KR01_FIT01", //fresh water consumption
                //..........Working Hour
                "FAC_JAL_001:SYM_.BALLMILL.CPU 414-3 DP.120MI02-ME01_Real_time", // work time => ballmill
                //---------- PLC Pulse
                "FAC_JAL_001:SYM_.BALLMILL.CPU 414-3 DP.CPU_Pulse_1_0Sec", // PLC Pulse of BallMill
                "FAC_JAL_001:SYM_.HPGR.CPU 414-3 DP.CPUH_Pulse_1_0Sec" // PLC Pulse of HPGR
            ]
        ]
        
        Alamofire.request(URL_GET_SelectedPoint, method: .post, parameters: queryString, encoding: URLEncoding(destination: .queryString), headers: HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                
                do {
                    if let json = try JSON(data: data).array {
                        for item in json {
                            let name = item["name"].stringValue
                            let quality = item["quality"].stringValue
                            let timestamp = item["timestamp"].stringValue
                            let value = item["value"].stringValue
                            let pointlist = PointsModel(pointName: name, pointValue: value,
                                                        pointQuality: quality, pointTimeStamp: timestamp)
                            self.pointsList.append(pointlist)
                        }
                        completion(true)
                    }
                } catch {
                    completion(false)
                    debugPrint(response.result.error as Any)
                }
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
}
