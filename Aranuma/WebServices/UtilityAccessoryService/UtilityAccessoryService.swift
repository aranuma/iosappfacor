//
//  UA_Service.swift
//  Aranuma
//
//  Created by topcoder on 11/20/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class UtilityAccessoryService {
    
    static let instance = UtilityAccessoryService()
    
    var pointsList = [PointsModel]()
    
    func GetPoints (completion: @escaping CompletionHandler) {
        
        let queryString: Parameters = [
            "pointName": [
                "FAC_JAL_001:SYM_.BALLMILL.CPU 414-3 DP.150KR01_LIT01",
                "FAC_JAL_001:SYM_.BALLMILL.CPU 414-3 DP.150KR01_LIT02",
                "FAC_JAL_001:SYM_.BALLMILL.CPU 414-3 DP.150TA01_LIT01",
                "FAC_JAL_001:SYM_.BALLMILL.CPU 414-3 DP.150TA01_LIT02"
            ]
        ]
        
        Alamofire.request(URL_GET_SelectedPoint, method: .post, parameters: queryString,
                          encoding: URLEncoding(destination: .queryString), headers: HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                do {
                    if let json = try JSON(data: data).array {
                        for item in json {
                            let name = item["name"].stringValue
                            let quality = item["quality"].stringValue
                            let timestamp = item["timestamp"].stringValue
                            let value = item["value"].stringValue
                            let point = PointsModel(pointName: name, pointValue: value,
                                                        pointQuality: quality, pointTimeStamp: timestamp)
                            self.pointsList.append(point)
                        }
                        completion(true)
                    }
                } catch {
                    completion(false)
                    debugPrint(response.result.error as Any)
                }
                
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
}
