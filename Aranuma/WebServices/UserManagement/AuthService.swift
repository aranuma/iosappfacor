//
//  AuthService.swift
//  Aranuma
//
//  Created by topcoder on 11/4/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AuthService {
    
    static let instance = AuthService()
    
    var username: String!
    
    let defaults = UserDefaults.standard
    
    var isLoggedIn: Bool {
        get {
            return defaults.bool(forKey: LOGGED_IN_KEY)
        }
        set {
            defaults.set(newValue, forKey: LOGGED_IN_KEY)
        }
    }
    
    var UserId: Int {
        get {
            return defaults.integer(forKey: USER_ID)
        }
        set {
            defaults.set(newValue, forKey: USER_ID)
        }
    }
    
    var UserToken: String {
        get {
            return (defaults.string(forKey: USER_TOKEN) ?? nil)!
        } set {
            defaults.set(newValue, forKey: USER_TOKEN)
        }
    }
    
    func UserLogin(username: String, password: String, completion: @escaping CompletionHandler) {
        let lowerCaseUsername = username.lowercased()
        
        let body: [String: Any] = [
            "username": lowerCaseUsername,
            "password": password
        ]
        
        Alamofire.request(URL_LOGIN, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseJSON { (response) in
            if response.result.error == nil {
                if let json = response.result.value as? Dictionary<String, Any> {
                    if let token = json["token"] as? String {
                        self.UserToken = token
                    }
                    
                    if let userid = json["Id"] as? Int {
                        self.UserId = userid
                    }
                    
                    if self.UserId != 0 {
                        self.isLoggedIn = true
                        completion(true)
                    } else {
                        self.isLoggedIn = false
                        completion(false)
                    }
                } else {
                    self.UserId = 0
                    self.isLoggedIn = false
                    completion(false)
                }
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
}
