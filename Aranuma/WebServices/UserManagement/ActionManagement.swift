//
//  ActionManagement.swift
//  Aranuma
//
//  Created by topcoder on 12/23/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class UserActions {
    static let instance = UserActions()
    
    var tokenIsValid: Bool!
    
    func SetSession(token: String, _session: String, completion: @escaping CompletionHandler) {
     
        var urlComponent = URLComponents(string: URL_SET_SESSION)
        
        let queryStringParam  =  [
            "token": token
        ]
        
        let queryItems = queryStringParam.map  { URLQueryItem(name: $0.key, value: $0.value) }
        urlComponent?.queryItems = queryItems
        
        let param = [
            "session": _session
        ]
        
        let headers = HEADER
        var request = URLRequest(url: urlComponent!.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: param)
        request.allHTTPHeaderFields = headers
        
        Alamofire.request(request).responseJSON {
            (response) in
            
            if response.result.error == nil {
                if let result = response.result.value as? Bool {
                    if result {
                        debugPrint("Session Added to database successfully \(result)")
                    } else {
                        debugPrint("Session is added so we can not add it again \(result)")
                    }
                } else {
                    debugPrint("Something is wrong when I wanna add the session")
                }
                completion(true)
            } else {
                debugPrint("Error On Session section")
                completion(false)
            }
        }
    }
    
    func IsTokenValid (token: String, completion: @escaping CompletionHandler) {
        
        let body: [String: Any] = [
            "token": token
        ]
        
         Alamofire.request(URL_IS_TOKENVALID, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseJSON {
            (response) in
            if response.result.error == nil {
                if let result = response.result.value as? Bool {
                    self.tokenIsValid = result
                }
                completion(true)
            } else {
                completion(false)
                self.tokenIsValid = false
            }
        }
    }
    
    func SetUserLog(token: String, eventLog: String, completion: @escaping CompletionHandler) {
        
        var urlComponent = URLComponents(string: URL_SET_UserLog)
        
        let queryStringParam  =  [
            "token": token
        ]
        
        let queryItems = queryStringParam.map  { URLQueryItem(name: $0.key, value: $0.value) }
        urlComponent?.queryItems = queryItems
        
        let param = [
            "eventLog": eventLog
        ]
        
        let headers = HEADER
        var request = URLRequest(url: urlComponent!.url!)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject: param)
        request.allHTTPHeaderFields = headers
        
        Alamofire.request(request).responseJSON {
            (response) in
            
            if response.result.error == nil {
                if let result = response.result.value as? Bool {
                    if result {
                        debugPrint("User Log Added to database successfully \(result)")
                    } else {
                        debugPrint("User can not add it  \(result)")
                    }
                } else {
                    debugPrint("Something is wrong when I wanna add the UserLog")
                }
                completion(true)
            } else {
                debugPrint("Error On Add UserLog section")
                completion(false)
            }
        }
    }
    
    var appVersion: String!
    func GetAppVersion(completion: @escaping CompletionHandler) {
        Alamofire.request(URL_GET_APPVeriosn, method: .get, encoding: URLEncoding(destination: .queryString), headers: HEADER).responseJSON { (response) in
            if response.result.error == nil {
                if let result = response.result.value as? String {
                    self.appVersion = result
                }
                completion(true)
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
    
}
