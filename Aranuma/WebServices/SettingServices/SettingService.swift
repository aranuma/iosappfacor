//
//  SettingService.swift
//  Aranuma
//
//  Created by topcoder on 1/2/19.
//  Copyright © 2019 aranuma. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class SettingServices {
    
    static let instance = SettingServices()
    
    var Sessions = [SessionModel]()
    var UsersLog = [UserLogModel]()
    
    public func GetAllSessions(token: String, completion: @escaping CompletionHandler) -> Void {
        let queryString: Parameters = [
            "token": token
        ]
        
        self.Sessions.removeAll()
        
        Alamofire.request(URL_GET_GetAllSessionForUsers, method: .get, parameters: queryString, headers: HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                
                do {
                    if let json = try JSON(data: data).array {
                        for item in json {
                            let sessionModel = SessionModel(session: item["session"].stringValue)
                            self.Sessions.append(sessionModel)
                        }
                        completion(true)
                    }
                } catch {
                    completion(false)
                    debugPrint(response.result.error as Any)
                }
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
        
    }
    
    public func GetAllLogs(token: String, completion: @escaping CompletionHandler) -> Void {
        
        UsersLog.removeAll()
        
        let body: [String: Any] = [
            "token": token
        ]
        
        Alamofire.request(URL_GET_AllUserLogs, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                
                do {
                    if let json = try JSON(data: data).array {
                        for item in json {
                            let logModel = UserLogModel(_event: item["_event"].stringValue, time: item["time"].stringValue)
                            self.UsersLog.append(logModel)
                        }
                        completion(true)
                    }
                } catch {
                    completion(false)
                    debugPrint(response.result.error as Any)
                }
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
    
    var PasswordChanged: Bool = false
    
    public func ChangePassword(token: String, password: String, completion: @escaping CompletionHandler) -> Void {
        
        let body: [String: Any] = [
            "token": token,
            "newpassword": password
        ]
        Alamofire.request(URL_POST_ChangePassword, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseJSON { (response) in
            if response.result.error != nil {
                self.PasswordChanged = true
                completion(true)
                
            } else {
                self.PasswordChanged = false
                completion(true)
            }
        }
    }
}
