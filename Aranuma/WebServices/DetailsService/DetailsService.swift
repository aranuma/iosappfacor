//
//  DetailsService.swift
//  Aranuma
//
//  Created by topcoder on 12/22/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class DetailsService {
    
    static let instance = DetailsService()
    
    var detailsResult = [CategoryDetailPointModel]()
    
    func GetDetialOfDevices(token: String, device: String, completion: @escaping CompletionHandler) {
        
        detailsResult.removeAll()
        
        let queryString: Parameters = [
            "token": token,
            "category": device
        ]
        
        Alamofire.request(URL_GET_GetAllListedPoints, method: .get, parameters: queryString, headers: HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                
                do {
                    if let json = try JSON(data: data).array {
                        for item in json {
                            let name = item["name"].stringValue
                            let quality = item["quality"].stringValue
                            let timestamp = item["timestamp"].stringValue
                            let value = item["value"].stringValue
                            let aliesname = item["aliesname"].stringValue
                            
                            let pointlist = CategoryDetailPointModel(pointName: name, pointValue: value, pointQuality: quality, pointTimeStamp: timestamp, pointAliesName: aliesname)
                            self.detailsResult.append(pointlist)
                        }
                        completion(true)
                    }
                } catch {
                    completion(false)
                    debugPrint(response.result.error as Any)
                }
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
    
    func GetAllPointForFiltering(token: String, completion: @escaping CompletionHandler) {
        
        detailsResult.removeAll()
        
        let queryString: Parameters = [
            "token": token,
        ]
        
        Alamofire.request(URL_GET_ALLPointForFilter, method: .get, parameters: queryString, headers: HEADER).responseJSON { (response) in
            if response.result.error == nil {
                guard let data = response.data else {return}
                
                do {
                    if let json = try JSON(data: data).array {
                        for item in json {
                            let name = item["fullname"].stringValue

                            let quality = item["quality"].stringValue
                            let timestamp = item["timestamp"].stringValue
                            let value = item["value"].stringValue
                            let aliesname = item["aliesname"].stringValue
                            
                            let pointlist = CategoryDetailPointModel(pointName: name, pointValue: value, pointQuality: quality, pointTimeStamp: timestamp, pointAliesName: aliesname)
                            self.detailsResult.append(pointlist)
                        }
                        completion(true)
                    }
                } catch {
                    completion(false)
                    debugPrint(response.result.error as Any)
                }
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
    
    func GetAllSelectedPointByFiltering(token: String, selectedItems: [String], completion: @escaping CompletionHandler) {
        
        detailsResult.removeAll()
        
        var urlComponent = URLComponents(string: URL_GET_ALLSelectedPoints)
        
        let queryStringParam  =  [
            "token": token
        ]
        
        let queryItems = queryStringParam.map { URLQueryItem(name: $0.key, value: $0.value) }
        urlComponent?.queryItems = queryItems

        let headers = HEADER
        var request = URLRequest(url: urlComponent!.url!)
        request.httpMethod = "POST"
        request.httpBody = try! JSONSerialization.data(withJSONObject: selectedItems)
        request.allHTTPHeaderFields = headers
        
        Alamofire.request(request).responseJSON {
            (response) in
            
            if response.result.error == nil {
                guard let data = response.data else {return}
                
                do {
                    if let json = try JSON(data: data).array {
                        for item in json {
                            let name = item["fullname"].stringValue
                            
                            let quality = item["quality"].stringValue
                            let timestamp = item["timestamp"].stringValue
                            let value = item["value"].stringValue
                            let aliesname = item["aliesname"].stringValue
                            
                            let pointlist = CategoryDetailPointModel(pointName: name, pointValue: value, pointQuality: quality, pointTimeStamp: timestamp, pointAliesName: aliesname)
                            self.detailsResult.append(pointlist)
                        }
                        completion(true)
                    } else {
                        print("Error: " + response.description)
                        debugPrint(response.result.error as Any)
                    }
                } catch {
                    debugPrint(response.result.error as Any)
                    completion(false)
                }
            } else {
                completion(false)
                print("I am here 2")
                debugPrint(response.result.error as Any)
            }
        }
    }
}
