//
//  AboutPopupVC.swift
//  Aranuma
//
//  Created by topcoder on 12/24/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import UIKit

class AboutPopupVC: UIViewController {

    @IBOutlet weak var btnExit: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func unwindPopUp(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
