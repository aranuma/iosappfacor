//
//  ShowSessionsVC.swift
//  Aranuma
//
//  Created by topcoder on 1/2/19.
//  Copyright © 2019 aranuma. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ShowSessionsVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var sessionCollection: UICollectionView!
    var activityIndicator : NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sessionCollection.delegate = self
        self.sessionCollection.dataSource = self
        
        let xAxis = self.view.frame.size.width / 2
        let yAxis = self.view.frame.size.height / 2
        
        let frame = CGRect(x: xAxis - 25, y: yAxis - 25, width: 50, height: 50)
        
        activityIndicator = NVActivityIndicatorView(frame: frame)
        activityIndicator.type = .ballRotateChase
        activityIndicator.color = #colorLiteral(red: 0.5882352941, green: 0, blue: 0.1490196078, alpha: 1)
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        FillSessionTable()
    }
    
    func FillSessionTable() {
        DispatchQueue.global(qos: .background).async {
            SettingServices.instance.GetAllSessions(token: AuthService.instance.UserToken, completion: {
                (success) in
                OperationQueue.main.addOperation({
                    if success {
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.removeFromSuperview()
                        self.sessionCollection.reloadData()
                    } else {
                        self.FillSessionTable()
                    }
                })
            })
        }
    }
    
    @IBAction func btnClose_Click(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SettingServices.instance.Sessions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = sessionCollection.dequeueReusableCell(withReuseIdentifier: "SettingUserSessionCells", for: indexPath) as? SettingUserSessionCells {
            let session = SettingServices.instance.Sessions[indexPath.row]
            cell.Config(session: session)
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
}
