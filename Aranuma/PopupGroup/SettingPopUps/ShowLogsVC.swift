//
//  ShowLogsVC.swift
//  Aranuma
//
//  Created by topcoder on 1/2/19.
//  Copyright © 2019 aranuma. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ShowLogsVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView_Logs: UICollectionView!
    var activityIndicator : NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView_Logs.delegate = self
        self.collectionView_Logs.dataSource = self
        
        let xAxis = self.view.frame.size.width / 2
        let yAxis = self.view.frame.size.height / 2
        
        let frame = CGRect(x: xAxis - 25, y: yAxis - 25, width: 50, height: 50)
        
        activityIndicator = NVActivityIndicatorView(frame: frame)
        activityIndicator.type = .ballRotateChase
        activityIndicator.color = #colorLiteral(red: 0.5882352941, green: 0, blue: 0.1490196078, alpha: 1)
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        FillSessionTable()
    }
    
    @IBAction func btnClose_Click(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func FillSessionTable() {
        DispatchQueue.global(qos: .background).async {
            SettingServices.instance.GetAllLogs(token: AuthService.instance.UserToken, completion: {
                (success) in
                OperationQueue.main.addOperation({
                    if success && SettingServices.instance.UsersLog.isEmpty == false {
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.removeFromSuperview()
                        self.collectionView_Logs.reloadData()
                    } else {
                        self.FillSessionTable()
                    }
                })
            })
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return SettingServices.instance.UsersLog.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView_Logs.dequeueReusableCell(withReuseIdentifier: "SettingUserLogCellsCollectionViewCell", for: indexPath) as? SettingUserLogCellsCollectionViewCell {
            let logs = SettingServices.instance.UsersLog[indexPath.row]
            cell.ConfigCell(logs: logs)
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
}
