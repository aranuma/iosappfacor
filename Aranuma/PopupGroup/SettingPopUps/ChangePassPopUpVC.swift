//
//  ChangePassPopUpVC.swift
//  Aranuma
//
//  Created by topcoder on 1/2/19.
//  Copyright © 2019 aranuma. All rights reserved.
//

import UIKit

class ChangePassPopUpVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtInput_CurrentPassword: CustomTextField_Login!
    @IBOutlet weak var txtInput_NewPassword: CustomTextField_Login!
    @IBOutlet weak var txtInput_ConfirmPassword: CustomTextField_Login!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func btnSubmit_Click(_ sender: Any) {
        if let oldpassword = self.txtInput_CurrentPassword.text, let newpassword = self.txtInput_NewPassword.text, let confirmnewpassword = self.txtInput_ConfirmPassword.text {
            if oldpassword != "" && newpassword != "" {
                if newpassword == confirmnewpassword {
                    DispatchQueue.global(qos: .background).async {
                        
                        AuthService.instance.UserLogin(username: AuthService.instance.username ?? "", password: oldpassword , completion: {
                            (success) in
                            if success && AuthService.instance.isLoggedIn {
                                SettingServices.instance.ChangePassword(token: AuthService.instance.UserToken, password: confirmnewpassword, completion: {
                                    (success) in
                                })
                                self.ShowAlert(message: "Password changed successfully")
                            } else {
                                self.ShowAlert(message: "username or password is wrong")
                            }
                        })
                    }
                } else {
                     self.ShowAlert(message: "check your new password with confirm password")
                }
            } else {
                self.ShowAlert(message: "please fill all feilds")
            }
        }
    }
    
    private func ShowAlert(message: String) -> Void {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnClose_Click(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}
