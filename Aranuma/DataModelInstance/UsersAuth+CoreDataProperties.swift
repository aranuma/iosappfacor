//
//  UsersAuth+CoreDataProperties.swift
//  Aranuma
//
//  Created by topcoder on 1/3/19.
//  Copyright © 2019 aranuma. All rights reserved.
//
//

import Foundation
import CoreData


extension UsersAuth {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UsersAuth> {
        return NSFetchRequest<UsersAuth>(entityName: "UsersAuth")
    }

    @NSManaged public var token: String?
    @NSManaged public var username: String?

}
