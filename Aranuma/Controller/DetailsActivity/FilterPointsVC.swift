//
//  FilterPointsVC.swift
//  Aranuma
//
//  Created by topcoder on 2/12/19.
//  Copyright © 2019 aranuma. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class FilterPointsVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var btnSortItems: UIButton!
    @IBOutlet weak var btnFilterItems: UIButton!
    @IBOutlet weak var btnAutoRefresh: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    
    @IBOutlet weak var buttonsCard: CardView!
    
    @IBOutlet weak var tableView_PointsDetail: UITableView!
    
    var selectedPointForRefresh = [String]()
    
    var activityIndicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView_PointsDetail.dataSource = self
        self.tableView_PointsDetail.delegate = self
        
        InitControls()
    }
    
    func InitControls() {
        self.btnSortItems.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
        self.btnSearch.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
        self.btnAutoRefresh.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
        self.btnFilterItems.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
        
        self.textField_FilterPhrase = UITextField(frame: CGRect(x: 10, y: 10, width: UIScreen.main.bounds.width - 20, height: 30))
        self.textField_FilterPhrase.isHidden = true
        self.textField_FilterPhrase.isEnabled = false
        self.view.addSubview(textField_FilterPhrase)
        self.FillListOfSelectedCategory(token: AuthService.instance.UserToken)
        self.textField_FilterPhrase.removeFromSuperview()
    }
    
    func LoadingView() {
        
        let xAxis = self.view.frame.size.width / 2
        let yAxis = self.view.frame.size.height / 2
        
        let frame = CGRect(x: xAxis - 25, y: yAxis - 25, width: 50, height: 50)
        activityIndicator = NVActivityIndicatorView(frame: frame)
        activityIndicator.type = .ballRotateChase
        activityIndicator.color = #colorLiteral(red: 0.5882352941, green: 0, blue: 0.1490196078, alpha: 1)
        
        self.activityIndicator.removeFromSuperview()
        if !self.view.subviews.contains(self.activityIndicator) {
            self.view.addSubview(self.activityIndicator)
        }
        
        activityIndicator.startAnimating()
    }

    @IBAction func unwindFromMainEquipmentVC(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DetailsService.instance.detailsResult.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView_PointsDetail.dequeueReusableCell(withIdentifier: "AllPointDetailFilter", for: indexPath) as? AllPointDetailFilter {
            if DetailsService.instance.detailsResult.isEmpty == false {
                
                if self.sortedBy_SelectedItem == "time asc" {
                    DetailsService.instance.detailsResult =
                        DetailsService.instance.detailsResult.sorted(by: {$0.pointTimeStamp > $1.pointTimeStamp})
                } else if self.sortedBy_SelectedItem == "time desc" {
                    DetailsService.instance.detailsResult =
                        DetailsService.instance.detailsResult.sorted(by: {$0.pointTimeStamp < $1.pointTimeStamp})
                } else if self.sortedBy_SelectedItem == "value asc" {
                    DetailsService.instance.detailsResult =
                        DetailsService.instance.detailsResult.sorted(by: {(Double($0.pointValue.components(separatedBy: ".")[0]) ?? 0) > (Double($1.pointValue.components(separatedBy: ".")[0]) ?? 0)})
                } else if self.sortedBy_SelectedItem == "value desc" {
                    DetailsService.instance.detailsResult =
                        DetailsService.instance.detailsResult.sorted(by: {(Double($0.pointValue.components(separatedBy: ".")[0]) ?? 0) < (Double($1.pointValue.components(separatedBy: ".")[0]) ?? 0)})
                } else if self.sortedBy_SelectedItem == "name asc" {
                    DetailsService.instance.detailsResult =
                        DetailsService.instance.detailsResult.sorted(by: {$0.pointName > $1.pointName})
                } else if self.sortedBy_SelectedItem == "name desc" {
                    DetailsService.instance.detailsResult =
                        DetailsService.instance.detailsResult.sorted(by: {$0.pointName < $1.pointName})
                } else if self.sortedBy_SelectedItem == "quality asc" {
                    DetailsService.instance.detailsResult =
                        DetailsService.instance.detailsResult.sorted(by: {$0.pointQuality > $1.pointQuality})
                } else if self.sortedBy_SelectedItem == "quality desc" {
                    DetailsService.instance.detailsResult =
                        DetailsService.instance.detailsResult.sorted(by: {$0.pointQuality < $1.pointQuality})
                }
                
                if indexPath.section < DetailsService.instance.detailsResult.count && indexPath.row < DetailsService.instance.detailsResult.count {
                    let point = DetailsService.instance.detailsResult[indexPath.row]
                    cell.ConfigCells(point: point)
                    return cell
                } else {
                    self.FillListOfSelectedCategory(token: AuthService.instance.UserToken)
                    return UITableViewCell()
                }
                
            } else {
                return cell
            }
        } else {
            return UITableViewCell()
        }
    }
    
    func FillListOfSelectedCategory(token: String) {
        
        if self.activityIndicator != nil {
            self.activityIndicator.removeFromSuperview()
        }
        
        self.LoadingView()
        
        DetailsService.instance.detailsResult.removeAll()
        
        DispatchQueue.global(qos: .background).async {
            DetailsService.instance.GetAllPointForFiltering(token: token, completion: {
                (success) in
                OperationQueue.main.addOperation({
                    if success && DetailsService.instance.detailsResult.isEmpty == false {
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.removeFromSuperview()
                        
                        if self.textField_FilterPhrase.text != "" {
                            var filteredList = [CategoryDetailPointModel]()

                            for item in DetailsService.instance.detailsResult {
                                if item.pointName.lowercased().contains(self.filterPhraseString.lowercased()) || item.pointAliesName.lowercased().contains(self.filterPhraseString.lowercased()) {
                                    filteredList.append(item)
                                }
                            }

                            var clearedList = [CategoryDetailPointModel]()
                            clearedList.removeAll()
                            for item in filteredList {
                                let itemExist = clearedList.contains(where: {$0.pointAliesName == item.pointAliesName})

                                if itemExist == false {
                                    clearedList.append(item)
                                }
                            }

                            if self.selectedPointForRefresh.isEmpty == false {
                                self.selectedPointForRefresh.removeAll()
                            }
                            
                            for item in clearedList {
                                self.selectedPointForRefresh.append(item.pointName)
                            }
                            
                            
                            DetailsService.instance.detailsResult.removeAll()
                            DetailsService.instance.detailsResult = clearedList

                            filteredList.removeAll()
                            clearedList.removeAll()

                            self.tableView_PointsDetail.reloadData()
                        } else {
                            self.tableView_PointsDetail.reloadData()
                        }
                        
                    } else {
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.removeFromSuperview()
                        self.FillListOfSelectedCategory(token: token)
                    }
                })
            })
        }
    }
    
    var cardView_ExpandSort: CardView!
    var show_CardView_ExpandSort: Bool = false
    var radioButton_SortByTime: UISegmentedControl!
    var radioButton_SortByName: UISegmentedControl!
    var radioButton_SortByValue: UISegmentedControl!
    var radioButton_SortByQuality: UISegmentedControl!
    
    var sortedBy_SelectedItem: String! = nil
    
    @IBAction func btnSortItems_onClick(_ sender: UIButton) {
        
        if self.showCardView_ExpandFilter == true {
            self.cardView_ExpandFilter.removeFromSuperview()
            self.btnFilterItems.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
            self.showCardView_ExpandFilter = false
        } else if self.showCardView_ExpandSearch == true {
            self.cardView_ExpandSearch.removeFromSuperview()
            self.btnSearch.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
            self.showCardView_ExpandSearch = false
        }
        
        if self.show_CardView_ExpandSort == false {
            btnSortItems.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
            
            let buttonCard: CGRect = buttonsCard.frame
            
            self.cardView_ExpandSort = CardView(frame: CGRect(x: buttonCard.origin.x,
                                                              y: buttonCard.origin.y + 50,
                                                              width: UIScreen.main.bounds.width, height: 225))
            self.cardView_ExpandSort.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
            self.cardView_ExpandSort.tag = 1
            
            //translationX: 0, y: -200 ===> scaleX: 0, y: 0
            //self.cardView_ExpandSort.transform = CGAffineTransform(translationX: 0, y: -200).concatenating(CGAffineTransform(scaleX: 0, y: 0))
            self.cardView_ExpandSort.transform = CGAffineTransform(translationX: 0, y: -15)
            
            CardView.animate(withDuration: 1, delay: 0.1, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
                self.cardView_ExpandSort.transform = .identity
            } ,completion: nil)
            
            // create controlls for cardview of sort button
            
            self.radioButton_SortByTime = UISegmentedControl(items: ["Timestamp Ascending", "Timestamp Descending"])
            self.radioButton_SortByTime.frame = CGRect(x: 0, y: 5, width: UIScreen.main.bounds.width, height: 50)
            self.radioButton_SortByTime.addTarget(self, action: #selector(self.radioBtn_SortByTime_onClick), for: .valueChanged)
            
            self.radioButton_SortByName = UISegmentedControl(items: ["Name Ascending", "Name Descending"])
            self.radioButton_SortByName.frame = CGRect(x: 0, y: 60, width: UIScreen.main.bounds.width, height: 50)
            self.radioButton_SortByName.addTarget(self, action: #selector(self.radioBtn_SortByName_onClick), for: .valueChanged)
            
            self.radioButton_SortByValue = UISegmentedControl(items: ["Value Ascending", "Value Descending"])
            self.radioButton_SortByValue.frame = CGRect(x: 0, y: 115, width: UIScreen.main.bounds.width, height: 50)
            self.radioButton_SortByValue.addTarget(self, action: #selector(self.radioBtn_SortByValue_onClick), for: .valueChanged)
            
            self.radioButton_SortByQuality = UISegmentedControl(items: ["Good Quality", "Bad Quality"])
            self.radioButton_SortByQuality.frame = CGRect(x: 0, y: 170, width: UIScreen.main.bounds.width, height: 50)
            self.radioButton_SortByQuality.addTarget(self, action: #selector(self.radioBtn_SortByQuality_onClick), for: .valueChanged)
            
            self.cardView_ExpandSort.addSubview(radioButton_SortByTime)
            self.cardView_ExpandSort.addSubview(radioButton_SortByName)
            self.cardView_ExpandSort.addSubview(radioButton_SortByValue)
            self.cardView_ExpandSort.addSubview(radioButton_SortByQuality)
            
            //End
            
            
            self.view.addSubview(self.cardView_ExpandSort)
            
            self.show_CardView_ExpandSort = true
            
        } else {
            CardView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
                self.cardView_ExpandSort.alpha = 0
            } ,completion: nil)
            
            self.btnSortItems.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
            self.show_CardView_ExpandSort = false
        }
    }
    
    @IBAction func radioBtn_SortByTime_onClick(segControl: UISegmentedControl) {

        self.radioButton_SortByQuality.selectedSegmentIndex = -1
        self.radioButton_SortByValue.selectedSegmentIndex = -1
        self.radioButton_SortByName.selectedSegmentIndex = -1
        
        if segControl.selectedSegmentIndex == 0 {
            self.sortedBy_SelectedItem = "time asc"
            self.FillListOfSelectedCategory(token: AuthService.instance.UserToken)
        } else {
            self.sortedBy_SelectedItem = "time desc"
            self.FillListOfSelectedCategory(token: AuthService.instance.UserToken)
        }
        
        CardView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
            self.cardView_ExpandSort.alpha = 0
        } ,completion: nil)
        
        self.btnSortItems.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
        self.show_CardView_ExpandSort = false
    }
    
    @IBAction func radioBtn_SortByValue_onClick(segControl: UISegmentedControl) {
        self.radioButton_SortByQuality.selectedSegmentIndex = -1
        self.radioButton_SortByTime.selectedSegmentIndex = -1
        self.radioButton_SortByName.selectedSegmentIndex = -1
        
        if segControl.selectedSegmentIndex == 0 {
            self.sortedBy_SelectedItem = "value asc"
            self.FillListOfSelectedCategory(token: AuthService.instance.UserToken)
        } else {
            self.sortedBy_SelectedItem = "value desc"
            self.FillListOfSelectedCategory(token: AuthService.instance.UserToken)
        }
        
        CardView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
            self.cardView_ExpandSort.alpha = 0
        } ,completion: nil)
        self.btnSortItems.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
        self.show_CardView_ExpandSort = false
    }
    
    @IBAction func radioBtn_SortByName_onClick(segControl: UISegmentedControl) {
        self.radioButton_SortByQuality.selectedSegmentIndex = -1
        self.radioButton_SortByTime.selectedSegmentIndex = -1
        self.radioButton_SortByValue.selectedSegmentIndex = -1
        
        if segControl.selectedSegmentIndex == 0 {
            self.sortedBy_SelectedItem = "name asc"
            self.FillListOfSelectedCategory(token: AuthService.instance.UserToken)
        } else {
            self.sortedBy_SelectedItem = "name desc"
            self.FillListOfSelectedCategory(token: AuthService.instance.UserToken)
        }
        
        CardView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
            self.cardView_ExpandSort.alpha = 0
        } ,completion: nil)
        self.btnSortItems.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
        self.show_CardView_ExpandSort = false
    }
    
    @IBAction func radioBtn_SortByQuality_onClick(segControl: UISegmentedControl) {
        self.radioButton_SortByName.selectedSegmentIndex = -1
        self.radioButton_SortByTime.selectedSegmentIndex = -1
        self.radioButton_SortByValue.selectedSegmentIndex = -1
        
        if segControl.selectedSegmentIndex == 0 {
            self.sortedBy_SelectedItem = "quality asc"
            self.FillListOfSelectedCategory(token: AuthService.instance.UserToken)
        } else {
            self.sortedBy_SelectedItem = "quality desc"
            self.FillListOfSelectedCategory(token: AuthService.instance.UserToken)
        }
        
        CardView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
            self.cardView_ExpandSort.alpha = 0
        } ,completion: nil)
        self.btnSortItems.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
        self.show_CardView_ExpandSort = false
    }
    
    //=-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-=
    
    var cardView_ExpandFilter: CardView!
    var showCardView_ExpandFilter: Bool = false
    
    @IBAction func btnFilterItems_onClick(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Info", message: "Coming soon", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in }))
        self.present(alert, animated: true, completion: nil)
        
//        if self.show_CardView_ExpandSort == true {
//            self.cardView_ExpandSort.removeFromSuperview()
//            self.btnSortItems.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
//            self.show_CardView_ExpandSort = false
//        } else if self.showCardView_ExpandSearch == true {
//            self.cardView_ExpandSearch.removeFromSuperview()
//            self.btnSearch.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
//            self.showCardView_ExpandSearch = false
//        }
//
//        if self.showCardView_ExpandFilter == false {
//
//            btnFilterItems.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
//
//            let buttonCard: CGRect = buttonsCard.frame
//
//            self.cardView_ExpandFilter = CardView(frame: CGRect(x: buttonCard.origin.x + 10,
//                                                                y: (UIScreen.main.bounds.height / 2) - 250,
//                                                                width: UIScreen.main.bounds.width - 20, height: 500))
//            self.cardView_ExpandFilter.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            self.cardView_ExpandFilter.tag = 0
//
//            //translationX: 0, y: -200 ===> scaleX: 0, y: 0
//            self.cardView_ExpandFilter.transform = CGAffineTransform(translationX: 0, y: UIScreen.main.bounds.height + 500)
//
//            CardView.animate(withDuration: 1, delay: 0.1, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
//                self.cardView_ExpandFilter.transform = .identity
//            } ,completion: nil)
//
//            self.view.addSubview(self.cardView_ExpandFilter)
//
//            self.showCardView_ExpandFilter = true
//
//        } else {
//            CardView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
//                self.cardView_ExpandFilter.alpha = 0
//            } ,completion: nil)
//
//            self.btnFilterItems.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
//            self.showCardView_ExpandFilter = false
//
//            cardView_ExpandSort.removeFromSuperview()
//        }
    }
    
    //=-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-=
    
    var cardView_ExpandSearch: CardView!
    var showCardView_ExpandSearch: Bool = false
    var textField_FilterPhrase: UITextField! = nil
    
    var btnAutoRefreshClicked: Bool = false
    
    @IBAction func btnSearchItems_onClick(_ sender: UIButton) {
        
        if self.show_CardView_ExpandSort == true {
            self.cardView_ExpandSort.removeFromSuperview()
            self.btnSortItems.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
            self.show_CardView_ExpandSort = false
        } else if self.showCardView_ExpandFilter == true {
            self.cardView_ExpandFilter.removeFromSuperview()
            self.btnFilterItems.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
            self.showCardView_ExpandFilter = false
        }
        
        if timerStarted == true {
            self.timerUpdateTableView.invalidate()
        }
        
        if btnAutoRefreshClicked {
            btnAutoRefresh.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            btnAutoRefresh.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.01568627451, blue: 0.2705882353, alpha: 1)
            timerUpdateTableView.invalidate()
            btnAutoRefresh.setTitle("Auto Refresh", for: .normal)
            btnAutoRefresh.setTitleColor(UIColor.white, for: UIControlState.normal)
            timerStarted = false
        }
        
        self.FillListOfSelectedCategory(token: AuthService.instance.UserToken)
        
        if self.showCardView_ExpandSearch == false {
            
            btnSearch.backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
            
            let buttonCard: CGRect = buttonsCard.frame
            
            self.cardView_ExpandSearch = CardView(frame: CGRect(x: buttonCard.origin.x,
                                                                y: buttonCard.origin.y + 50,
                                                                width: UIScreen.main.bounds.width, height: 50))
            self.cardView_ExpandSearch.backgroundColor = #colorLiteral(red: 0.8862745098, green: 0.8862745098, blue: 0.8862745098, alpha: 1)
            self.cardView_ExpandSearch.tag = 1
            
            //translationX: 0, y: -200 ===> scaleX: 0, y: 0
            self.cardView_ExpandSearch.transform = CGAffineTransform(translationX: 0, y: -15)
            
            self.textField_FilterPhrase = UITextField(frame: CGRect(x: 10, y: 10, width: UIScreen.main.bounds.width - 20, height: 30))
            self.textField_FilterPhrase.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            self.textField_FilterPhrase.placeholder = "Filter phrase ..."
            self.textField_FilterPhrase.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.textField_FilterPhrase.delegate = self
            
            CardView.animate(withDuration: 1, delay: 0.1, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
                self.cardView_ExpandSearch.transform = .identity
            } ,completion: nil)
            
            self.view.addSubview(self.cardView_ExpandSearch)
            
            self.cardView_ExpandSearch.addSubview(textField_FilterPhrase)
            
            self.showCardView_ExpandSearch = true
            
        } else {
            CardView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
                self.cardView_ExpandSearch.alpha = 0
            } ,completion: nil)
            
            self.btnSearch.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
            self.showCardView_ExpandSearch = false
            
            cardView_ExpandSearch.removeFromSuperview()
        }
    }
    
    var filterPhraseString: String!
    @objc func textFieldDidChange(_ textField: UITextField) {
        // on textfield text change event handler
        filterPhraseString = textField.text
        
        if self.textField_FilterPhrase.text != "" {
            self.btnAutoRefresh.isEnabled = true
        } else {
            self.btnAutoRefresh.isEnabled = false
        }
        
        self.FillListOfSelectedCategory(token: AuthService.instance.UserToken)
    }
 
    //-=-==-=-=-=-==-=--==-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-==-==-=-=-=-=-=-
    var timerUpdateTableView = Timer()
    var timerStarted: Bool = false
    @IBAction func btnAutoRefresh_onClick(_ sender: UIButton) {
        
        if DetailsService.instance.detailsResult.count > 20 {
            let alert = UIAlertController(title: "Error", message: "Please select just 20 points for refresh", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in }))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        if timerStarted == false {
            timerUpdateTableView = Timer.scheduledTimer(timeInterval: 3.0, target: self,
                                        selector: #selector(AutoRefreshPoints), userInfo: nil, repeats: true)
            btnAutoRefresh.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            btnAutoRefresh.backgroundColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
            btnAutoRefresh.setTitle("STOP", for: .normal)
            btnAutoRefresh.setTitleColor(UIColor.white, for: UIControlState.normal)
            timerStarted = true
            btnAutoRefreshClicked = true
        } else {
            btnAutoRefresh.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            btnAutoRefresh.backgroundColor = #colorLiteral(red: 0.9529411765, green: 0.01568627451, blue: 0.2705882353, alpha: 1)
            timerUpdateTableView.invalidate()
            btnAutoRefresh.setTitle("Auto Refresh", for: .normal)
            btnAutoRefresh.setTitleColor(UIColor.white, for: UIControlState.normal)
            timerStarted = false
            btnAutoRefreshClicked = false
        }
    }
    
    @objc func AutoRefreshPoints() {
        if self.activityIndicator != nil {
            self.activityIndicator.removeFromSuperview()
        }
        
        if timerStarted == false {
            self.LoadingView()
        }
        
        DispatchQueue.global(qos: .background).async {
            DetailsService.instance.GetAllSelectedPointByFiltering(token: AuthService.instance.UserToken, selectedItems: self.selectedPointForRefresh, completion: {
                (success) in
                OperationQueue.main.addOperation({
                    if success && DetailsService.instance.detailsResult.isEmpty == false {
                        if self.activityIndicator != nil {
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.removeFromSuperview()
                        }
                        
                        if self.textField_FilterPhrase.text != "" {
                            var filteredList = [CategoryDetailPointModel]()
                            
                            for item in DetailsService.instance.detailsResult {
                                if item.pointName.lowercased().contains(self.filterPhraseString.lowercased()) || item.pointAliesName.lowercased().contains(self.filterPhraseString.lowercased()) {
                                    filteredList.append(item)
                                }
                            }
                            
                            var clearedList = [CategoryDetailPointModel]()
                            clearedList.removeAll()
                            for item in filteredList {
                                clearedList.append(item)
                            }
                            
                            DetailsService.instance.detailsResult.removeAll()
                            DetailsService.instance.detailsResult = clearedList
                            
                            filteredList.removeAll()
                            clearedList.removeAll()
                            
                            self.tableView_PointsDetail.reloadData()
                        } else {
                            self.tableView_PointsDetail.reloadData()
                        }
                        
                    } else {
                        self.AutoRefreshPoints()
                    }
                })
            })
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        CardView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
            self.cardView_ExpandSearch.alpha = 0
        } ,completion: nil)
        
        self.btnSearch.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
        self.showCardView_ExpandSearch = false
        timerStarted = false
        cardView_ExpandSearch.removeFromSuperview()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        CardView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
            self.cardView_ExpandSearch.alpha = 0
        } ,completion: nil)
        
        self.btnSearch.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.937254902, alpha: 1)
        self.showCardView_ExpandSearch = false
        timerStarted = false
        cardView_ExpandSearch.removeFromSuperview()
        return false
    }
    
}
