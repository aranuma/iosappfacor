//
//  DetialsVC.swift
//  Aranuma
//
//  Created by topcoder on 11/17/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import UIKit
//

class DetialsVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionViewHorizontalItems: UICollectionView!
    
    var categoryName: String!
    
    var detialsItemImage = [
        UIImage(named: "makeup_water_detial_page"),
        UIImage(named: "hpgr_detail_page"),
        UIImage(named: "ballmill_detail_page"),
        UIImage(named: "hydro_cyclone_detial_page"),
        UIImage(named: "drum_seperator_detail_page"),
        UIImage(named: "belt_filter_detial_page"),
        UIImage(named: "thickener_detial_page"),
        UIImage(named: "flocculant_detial_page"),
        UIImage(named: "conveyer_detail_page"),
        UIImage(named: "compressor_detail_page"),
        UIImage(named: "cooling_tower_detial_page"),
        UIImage(named: "motor_detail_page"),
        UIImage(named: "pump_detail_page"),
        UIImage(named: "pump_station_detial_page"),
        UIImage(named: "dry_grinding_detail_page"),
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionViewHorizontalItems.delegate = self
        self.collectionViewHorizontalItems.dataSource = self
        InitControls()
    }

    func InitControls() {}
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.detialsItemImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.collectionViewHorizontalItems.dequeueReusableCell(withReuseIdentifier: "ImageDetailsCollectionCellsCollectionViewCell", for: indexPath) as! ImageDetailsCollectionCellsCollectionViewCell
        
        cell.imageDetialsItem.image = detialsItemImage[indexPath.row]
        cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap(_:))))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenWidth = UIScreen.main.bounds.width
        let scaleFactor = (screenWidth / 3) - 6
        
        return CGSize(width: scaleFactor, height: scaleFactor)
    }
    
    @objc func tap(_ sender: UITapGestureRecognizer) {
        let location = sender.location(in: self.collectionViewHorizontalItems)
        let indexPath = self.collectionViewHorizontalItems.indexPathForItem(at: location)

        
        if let index = indexPath {
            switch(index[1]) {
            case 0:
                do {
                self.categoryName = "MakeUp"
                break
                }
            case 1:
                do {
                self.categoryName = "HPGR"
                break
                }
            case 2: do {
                self.categoryName = "Ball Mill"
                break
                }
            case 3: do {
                self.categoryName = "Hydro Cyclon"
                break
                }
            case 4: do {
                self.categoryName = "Drum Separator"
                break
                }
            case 5: do {
                self.categoryName = "Belt Filter"
                break
                }
            case 6: do {
                self.categoryName = "Thickener"
                break
                }
            case 7: do {
                self.categoryName = "Flocculant"
                break
                }
            case 8: do {
                self.categoryName = "Conveyor"
                break
                }
            case 9: do {
                self.categoryName = "Compressor"
                break
                }
            case 10: do {
                self.categoryName = "Cooling Tower"
                break
                }
            case 11: do {
                self.categoryName = "Motor"
                break
                }
            case 12: do {
                self.categoryName = "Pump"
                break
                }
            case 13: do {
                self.categoryName = "Pump Station"
                break
                }
            case 14: do {
                self.categoryName = "Dry Grinding"
                break
                }
            default:
                return
            }
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "FilterPointsWithCategoryVC") as! FilterPointsWithCategoryVC
            newViewController.categoryName = self.categoryName
            self.present(newViewController, animated: true, completion: nil)
        }
    }
    
}
