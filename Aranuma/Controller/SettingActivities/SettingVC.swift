//
//  SettingVC.swift
//  Aranuma
//
//  Created by topcoder on 12/26/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import UIKit
import CoreData

class SettingVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet var setting_CollectionItems: UICollectionView!
    
    let settingItem = [
            SettingModel(title: "Change password", description: "Change your password here"),
            SettingModel(title: "Logout", description: "Logout from your current account"),
            SettingModel(title: "Sessions", description: "Look at your current and pervious sessions"),
            SettingModel(title: "Logs", description: "Look at your activity logs")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.InitControlls()
    }
    
    private func InitControlls() -> Void {
        self.setting_CollectionItems.delegate = self
        self.setting_CollectionItems.dataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.settingItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.setting_CollectionItems.dequeueReusableCell(withReuseIdentifier: "SettingItemCell", for: indexPath) as! SettingItemCell
        cell.ConfigCells(setting: settingItem[indexPath.row])
        cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tap(_:))))
        return cell
    }
    
    @objc func tap(_ sender: UITapGestureRecognizer) {
        let location = sender.location(in: self.setting_CollectionItems)
        let indexPath = self.setting_CollectionItems.indexPathForItem(at: location)
        
        if let index = indexPath {
            if index[1] == 0 { //change password item
                performSegue(withIdentifier: "changepassword_view", sender: self)
            } else if index[1] == 1 {

                let exitAlert = UIAlertController(title: "Info", message: "Do realy want to logout?", preferredStyle: UIAlertControllerStyle.alert)
                
                exitAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                    let context = PersistanceService.persistentContainer.viewContext
                    
                    let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "UsersAuth")
                    let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
                    
                    do {
                        try context.execute(deleteRequest)
                        try context.save()
                    } catch {
                        print ("There was an error")
                    }
                    
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "LoginView") as! LoginViewController
                    self.present(newViewController, animated: true, completion: nil)
                }))
                
                exitAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                    debugPrint("Handle Cancel Logic here")
                }))
            
                self.present(exitAlert, animated: true, completion: nil)
                
                
            } else if index[1] == 2 {
                performSegue(withIdentifier: "sessions_settingview", sender: self)
            } else if index[1] == 3 {
                performSegue(withIdentifier: "showlogs_settingview", sender: self)
            }
        }
    }
}
