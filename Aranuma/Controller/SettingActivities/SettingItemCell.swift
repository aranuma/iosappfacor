//
//  SettingItemCell.swift
//  Aranuma
//
//  Created by topcoder on 1/1/19.
//  Copyright © 2019 aranuma. All rights reserved.
//

import UIKit

class SettingItemCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    func ConfigCells(setting: SettingModel) {
        lblTitle.text = setting.title
        lblDescription.text = setting.description
    }
}
