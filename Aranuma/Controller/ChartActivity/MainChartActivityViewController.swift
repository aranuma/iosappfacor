//
//  MainChartActivityViewController.swift
//  Aranuma
//
//  Created by topcoder on 11/26/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import UIKit
import iOSDropDown

class MainChartActivityViewController: UIViewController {

    @IBOutlet weak var dropDownSpinner_PointFamily: DropDown!
    @IBOutlet weak var dropDownSpinner_Point: DropDown!
    
    @IBOutlet weak var lblSelectDuration: UILabel!
    @IBOutlet weak var uiViewDuration: UIView!
    
    @IBOutlet weak var btn_1_WEEK_time: UIButton!
    @IBOutlet weak var btn_2HOURS_time: UIButton!
    @IBOutlet weak var btn_1HOUR_time: UIButton!
    @IBOutlet weak var btn_2DAYS_time: UIButton!
    @IBOutlet weak var btn_1DAY_time: UIButton!
    
    @IBOutlet weak var uiViewSelectChart: UIView!
    
    @IBOutlet weak var btn_LineChart: UIButton!
    @IBOutlet weak var btn_AreaChart: UIButton!
    @IBOutlet weak var btn_threedChart: UIButton!
    
    var sufix: String = ""
    var tblName: String = ""
    var time: String = ""
    var deviceCategory = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        InitControls()
    }
    
    func GetDeviceCategoryName() {
        InitChartPointsService.instance.GetDeviceCategories(token: AuthService.instance.UserToken, completion: {
            (success) in
            if success {
                if InitChartPointsService.instance.ChartCategories.isEmpty == false {
                    DispatchQueue.main.async(execute: { () -> Void in
                        
                        var deviceNames = [String]()
                        
                        for item in InitChartPointsService.instance.ChartCategories {
                            deviceNames.append(item.deviceName)
                            self.deviceCategory.append(item.deviceCategory)
                        }
                        
                        self.dropDownSpinner_PointFamily.optionArray = deviceNames
                    })
                }
            } else {
                print("error")
                self.GetDeviceCategoryName()
            }
        })
    }
    
    func InitControls() {
        
        lblSelectDuration.alpha = 0.0
        uiViewDuration.alpha = 0.0
        uiViewSelectChart.alpha = 0.0
        
        dropDownSpinner_PointFamily.isSearchEnable = false
        dropDownSpinner_Point.isSearchEnable = false
        
        GetDeviceCategoryName()
        
        dropDownSpinner_PointFamily.selectedRowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)

        dropDownSpinner_PointFamily.didSelect {
            (selectedText , index ,id) in
            
            self.dropDownSpinner_Point.selectedRowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            
            InitChartPointsService.instance.GetPointOfDevice(token: AuthService.instance.UserToken, category: selectedText, completion: { (success) in
                if success && InitChartPointsService.instance.ChartDevicePoints.isEmpty == false {
                    self.sufix = self.deviceCategory[index] + "_"
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.dropDownSpinner_Point.optionArray = InitChartPointsService.instance.ChartDevicePoints
                    })
                }
            })
        }
        
        dropDownSpinner_Point.didSelect {
            (selectedText, index, id) in
            self.tblName = (self.sufix + selectedText).replacingOccurrences(of: "-", with: "_")
            
            if self.lblSelectDuration.alpha == 0.0 {
                UILabel.animate(withDuration: 1, animations: {
                    self.lblSelectDuration.alpha = 1.0
                })
            }
            
            if self.uiViewDuration.alpha == 0.0 {
                UIView.animate(withDuration: 1, animations: {
                    self.uiViewDuration.alpha = 1.0
                })
            }
        }
    }
    
    @IBAction func btn_1_WEEK_OnClick(_ sender: Any) {
        self.btn_1_WEEK_time.setTitleColor(.green, for: .normal)
        self.btn_2HOURS_time.setTitleColor(.black, for: .normal)
        self.btn_2DAYS_time.setTitleColor(.black, for: .normal)
        self.btn_1HOUR_time.setTitleColor(.black, for: .normal)
        self.btn_1DAY_time.setTitleColor(.black, for: .normal)
        self.time = "1-week"
        if self.uiViewSelectChart.alpha == 0.0 {
            UIView.animate(withDuration: 1, animations: {
                self.uiViewSelectChart.alpha = 1.0
            })
        }
    }
    
    @IBAction func btn_2_HOURS_OnClick(_ sender: Any) {
        self.btn_2HOURS_time.setTitleColor(.green, for: .normal)
        self.btn_1_WEEK_time.setTitleColor(.black, for: .normal)
        self.btn_2DAYS_time.setTitleColor(.black, for: .normal)
        self.btn_1HOUR_time.setTitleColor(.black, for: .normal)
        self.btn_1DAY_time.setTitleColor(.black, for: .normal)
        self.time = "2-hour"
        if self.uiViewSelectChart.alpha == 0.0 {
            UIView.animate(withDuration: 1, animations: {
                self.uiViewSelectChart.alpha = 1.0
            })
        }
    }
    
    @IBAction func btn_1_HOUR_OnClick(_ sender: Any) {
        self.btn_1HOUR_time.setTitleColor(.green, for: .normal)
        self.btn_2HOURS_time.setTitleColor(.black, for: .normal)
        self.btn_1_WEEK_time.setTitleColor(.black, for: .normal)
        self.btn_2DAYS_time.setTitleColor(.black, for: .normal)
        self.btn_1DAY_time.setTitleColor(.black, for: .normal)
        self.time = "1-hour"
        if self.uiViewSelectChart.alpha == 0.0 {
            UIView.animate(withDuration: 1, animations: {
                self.uiViewSelectChart.alpha = 1.0
            })
        }
    }
    
    @IBAction func btn_2_DAYS_OnClick(_ sender: Any) {
        self.btn_2DAYS_time.setTitleColor(.green, for: .normal)
        self.btn_2HOURS_time.setTitleColor(.black, for: .normal)
        self.btn_1_WEEK_time.setTitleColor(.black, for: .normal)
        self.btn_1HOUR_time.setTitleColor(.black, for: .normal)
        self.btn_1DAY_time.setTitleColor(.black, for: .normal)
        self.time = "2-day"
        if self.uiViewSelectChart.alpha == 0.0 {
            UIView.animate(withDuration: 1, animations: {
                self.uiViewSelectChart.alpha = 1.0
            })
        }
    }
    
    @IBAction func btn_1_DAY_OnClick(_ sender: Any) {
        self.btn_1DAY_time.setTitleColor(.green, for: .normal)
        self.btn_2HOURS_time.setTitleColor(.black, for: .normal)
        self.btn_1_WEEK_time.setTitleColor(.black, for: .normal)
        self.btn_2DAYS_time.setTitleColor(.black, for: .normal)
        self.btn_1HOUR_time.setTitleColor(.black, for: .normal)
        self.time = "1-day"
        if self.uiViewSelectChart.alpha == 0.0 {
            UIView.animate(withDuration: 1, animations: {
                self.uiViewSelectChart.alpha = 1.0
            })
        }
    }
    
    @IBAction func btn_LineChart_OnClick(_ sender: Any) {
        performSegue(withIdentifier: "goto_LineChart_Activity", sender: self)
        RestControls()
    }
    
    @IBAction func btn_AreaChart_OnClick(_ sender: Any) {
        performSegue(withIdentifier: "goto_AreaChart_Activity", sender: self)
        RestControls()
    }
    
    
    @IBAction func btn_3DChrt_OnClick(_ sender: Any) {
        performSegue(withIdentifier: "goto_threedChart_Activity", sender: self)
        RestControls()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "goto_LineChart_Activity" {
            let vcLineChart = segue.destination as! LineChartVC
            vcLineChart.tblName = self.tblName
            vcLineChart.time = self.time
        } else if segue.identifier == "goto_AreaChart_Activity" {
            let vcAreaChart = segue.destination as! AreaChartVC
            vcAreaChart.tblName = self.tblName
            vcAreaChart.time = self.time
        } else if segue.identifier == "goto_threedChart_Activity" {
            let threedChartChart = segue.destination as! threedChartVC
            threedChartChart.tblName = self.tblName
            threedChartChart.time = self.time
        }
    }
    
    func RestControls() {
        self.btn_1DAY_time.setTitleColor(.black, for: .normal)
        self.btn_2HOURS_time.setTitleColor(.black, for: .normal)
        self.btn_1_WEEK_time.setTitleColor(.black, for: .normal)
        self.btn_2DAYS_time.setTitleColor(.black, for: .normal)
        self.btn_1HOUR_time.setTitleColor(.black, for: .normal)
        self.lblSelectDuration.alpha = 0.0
        self.uiViewDuration.alpha = 0.0
        self.uiViewSelectChart.alpha = 0.0
        self.dropDownSpinner_Point.text = ""
        self.dropDownSpinner_PointFamily.text = ""
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(false)
//        dropDownSpinner_Point.hideList()
//        dropDownSpinner_PointFamily.hideList()
    }
}
