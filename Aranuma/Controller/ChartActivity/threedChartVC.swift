//
//  threedChartVC.swift
//  Aranuma
//
//  Created by topcoder on 12/2/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Highcharts

class threedChartVC: UIViewController {
    
    @IBOutlet weak var uiViewhighchart_ChartPlaceHolder: UIView!
    
    @IBOutlet weak var lblMaximumValue: UILabel!
    @IBOutlet weak var lblMaximumTime: UILabel!
    @IBOutlet weak var lblMinimumValue: UILabel!
    @IBOutlet weak var lblMinimumTime: UILabel!
    
    @IBOutlet weak var lblAvarage: UILabel!
    @IBOutlet weak var lblMedian: UILabel!
    @IBOutlet weak var lblVariance: UILabel!
    
    var tblName: String = ""
    var time: String = ""
    
    var chartView: HIChartView!
    
    var activityIndicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        InitControlls()
    }
    
    func InitControlls() {
        let xAxis = self.view.frame.size.width / 2
        let yAxis = self.view.frame.size.height / 2
        
        let frame = CGRect(x: xAxis - 25, y: yAxis - 25, width: 50, height: 50)
        
        activityIndicator = NVActivityIndicatorView(frame: frame)
        activityIndicator.type = .ballRotateChase
        activityIndicator.color = #colorLiteral(red: 0.5882352941, green: 0, blue: 0.1490196078, alpha: 1)
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        self.chartView = HIChartView(frame: self.uiViewhighchart_ChartPlaceHolder.bounds)
        Draw_Chart_Points()
    }
    
    func Draw_Chart_Points() {
        DispatchQueue.global(qos: .background).async {
            GetChartDataService.instance.GetPoint(token: AuthService.instance.UserToken, requestPoint: self.tblName, time: self.time, completion: {
                (success) in
                if success {
                    if GetChartDataService.instance.pointValue.isEmpty == false {
                        self.InitChartOptions(listValue: GetChartDataService.instance.pointValue, listTime: GetChartDataService.instance.pointTimeStamp)
                        self.uiViewhighchart_ChartPlaceHolder.addSubview(self.chartView)
                        
                        let uiCover: UIView = UIView(frame: CGRect(x: 0, y: self.uiViewhighchart_ChartPlaceHolder.frame.size.height - 18, width: self.uiViewhighchart_ChartPlaceHolder.frame.size.width, height: 18))
                        uiCover.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                        
                        self.uiViewhighchart_ChartPlaceHolder.addSubview(uiCover)
                        uiCover.bringSubview(toFront: self.uiViewhighchart_ChartPlaceHolder)
                        
                        let maxValue: [Float32] = Calculate.instance.GetMaximum(arrayList: GetChartDataService.instance.pointValue)
                        
                        let minValue: [Float32] = Calculate.instance.GetMinimum(arrayList: GetChartDataService.instance.pointValue)
                        
                        self.lblMaximumValue.text = String(maxValue[0])
                        self.lblMaximumTime.text = String(GetChartDataService.instance.pointTimeStamp[Int(maxValue[1])])
                        
                        self.lblMinimumValue.text = String(minValue[0])
                        self.lblMinimumTime.text = String(GetChartDataService.instance.pointTimeStamp[Int(minValue[1])])
                        
                        self.lblAvarage.text = String(Calculate.instance.GetAverage(arrayList: GetChartDataService.instance.pointValue))
                        self.lblMedian.text = String(Calculate.instance.GetMedian(arrayList: GetChartDataService.instance.pointValue))
                        self.lblVariance.text = String(Calculate.instance.GetVariance(arrayList: GetChartDataService.instance.pointValue))
                        
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.removeFromSuperview()
                    } else {
                        self.self.ErrorDialog(message: "The item that you selected has not any points please select another one", title: "info")
                    }
                } else {
                    self.ErrorDialog(message: "We have problem on connection the server please try latter", title: "error")
                }
            })
        }
    }
    
    func ErrorDialog(message: String, title: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func InitChartOptions(listValue: [Float32], listTime: [String]){
        let options = HIOptions()

        let hiChart = HIChart()
        hiChart.type = "column"
        let hiOption3d = HIOptions3d()
        hiOption3d.enabled = true
        hiOption3d.alpha = 10
        hiOption3d.beta = 25
        hiOption3d.depth = 70
        hiChart.options3d = hiOption3d
        hiChart.zoomType = "x"
        options.chart = hiChart
        
        let hiExporting = HIExporting()
        hiExporting.enabled = false
        options.exporting = hiExporting
        
        let hiTitle = HITitle()
        hiTitle.text = ""
        options.title = hiTitle
        
        let plotOption = HIPlotOptions()
        let hiColumn = HIColumn()
        hiColumn.depth = 25
        plotOption.column = hiColumn
        options.plotOptions = plotOption
        
        let xAxis = HIXAxis()
        xAxis.categories = listTime
        let label_x = HILabels()
        label_x.skew3d = true
        let hiStyle = HIStyle()
        hiStyle.fontSize = "16px"
        label_x.style = hiStyle
        xAxis.labels = label_x
        options.xAxis = [xAxis]
        
        let yAxis = HIYAxis()
        let title_y = HITitle()
        title_y.text = ""
        yAxis.title = title_y
        options.yAxis = [yAxis]
        
        let series = HIColumn()
        series.name = "Value"
        series.data = listValue
        options.series = [series]
        
        self.chartView.options = options
    }

    @IBAction func btnBack_OnClick(_ sender: Any) {
        super.viewWillDisappear(false)
        self.dismiss(animated: true, completion: nil)
    }
}
