//
//  LineChartVC.swift
//  Aranuma
//
//  Created by topcoder on 11/27/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import UIKit
import Highcharts
import NVActivityIndicatorView

class LineChartVC: UIViewController, UITextFieldDelegate {

    var chartView: HIChartView!
    @IBOutlet weak var uiViewLineChart_PlaceHolder: CardView!
    
    var tblName: String = ""
    var time: String = ""
    
    @IBOutlet weak var lblMaximum: UILabel!
    @IBOutlet weak var lblTime_Max: UILabel!
    
    @IBOutlet weak var lblMinimum: UILabel!
    @IBOutlet weak var lblTime_Min: UILabel!
    
    @IBOutlet weak var lblAvarage: UILabel!
    @IBOutlet weak var lblMedian: UILabel!
    @IBOutlet weak var lblVariance: UILabel!
    
    var activityIndicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        InitControls()
    }
    
    func InitControls() {
        
        
        
        let xAxis = self.view.frame.size.width / 2
        let yAxis = self.view.frame.size.height / 2
        
        let frame = CGRect(x: xAxis - 25, y: yAxis - 25, width: 50, height: 50)
        activityIndicator = NVActivityIndicatorView(frame: frame)
        activityIndicator.type = .ballRotateChase
        activityIndicator.color = #colorLiteral(red: 0.5882352941, green: 0, blue: 0.1490196078, alpha: 1)
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        self.chartView = HIChartView(frame: self.uiViewLineChart_PlaceHolder.bounds)
        Draw_Chart_Points()
    }
    
    func Draw_Chart_Points() {
        DispatchQueue.global(qos: .background).async {
            GetChartDataService.instance.GetPoint(token: AuthService.instance.UserToken, requestPoint: self.tblName, time: self.time, completion: {
                (success) in
                if success {
                    if GetChartDataService.instance.pointValue.isEmpty == false {
                        self.InitChartOptions(listValue: GetChartDataService.instance.pointValue, listTime: GetChartDataService.instance.pointTimeStamp)
                        self.uiViewLineChart_PlaceHolder.addSubview(self.chartView)
                        
                        let uiCover: UIView = UIView(frame: CGRect(x: 0, y: self.uiViewLineChart_PlaceHolder.frame.size.height - 22, width: self.uiViewLineChart_PlaceHolder.frame.size.width, height: 22))
                        uiCover.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                        self.uiViewLineChart_PlaceHolder.addSubview(uiCover)
                        
                        uiCover.bringSubview(toFront: self.uiViewLineChart_PlaceHolder)
                        
                        let maxValue: [Float32] = Calculate.instance.GetMaximum(arrayList: GetChartDataService.instance.pointValue)
                        let minValue: [Float32] = Calculate.instance.GetMinimum(arrayList: GetChartDataService.instance.pointValue)
     
                        self.lblMaximum.text = String(maxValue[0])
                        self.lblTime_Max.text = String(GetChartDataService.instance.pointTimeStamp[Int(maxValue[1])])
                        print(String(GetChartDataService.instance.pointTimeStamp[Int(maxValue[1])]))
                    
                        self.lblMinimum.text = String(minValue[0])
                        self.lblTime_Min.text = String(GetChartDataService.instance.pointTimeStamp[Int(minValue[1])])
                        print(String(GetChartDataService.instance.pointTimeStamp[Int(minValue[1])]))
                        
                        self.lblAvarage.text = String(Calculate.instance.GetAverage(arrayList: GetChartDataService.instance.pointValue))
                        self.lblMedian.text = String(Calculate.instance.GetMedian(arrayList: GetChartDataService.instance.pointValue))
                        self.lblVariance.text = String(Calculate.instance.GetVariance(arrayList: GetChartDataService.instance.pointValue))
                        
                        self.activityIndicator.stopAnimating()
                        self.activityIndicator.removeFromSuperview()
                        
                    } else {
                        self.self.ErrorDialog(message: "The item that you selected has not any points please select another one", title: "info")
                    }
                } else {
                    self.ErrorDialog(message: "We have problem on connection the server please try latter", title: "error")
                }
            })
        }
    }
    
    func ErrorDialog(message: String, title: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func InitChartOptions(listValue: [Float32], listTime: [String]) {
        self.chartView.plugins = ["data"]

        let options = HIOptions()

        let hiExporting = HIExporting()
        hiExporting.enabled = false
        options.exporting = hiExporting

        let hiTitle = HITitle()
        hiTitle.text = ""
        options.title = hiTitle

        let chart = HIChart()
        chart.type = "line"
        chart.zoomType = "x"
        options.chart = chart

        let xAxis = HIXAxis()
        xAxis.tickInterval = 7 * 24 * 3600 * 1000 as NSNumber
        xAxis.tickWidth = 0
        xAxis.gridLineWidth = 1
        xAxis.categories = listTime

        let hiLabel = HILabels()
        hiLabel.align = "left"
        hiLabel.x = 3
        hiLabel.y = -3
        xAxis.labels = hiLabel

        options.xAxis = [xAxis]

        let yAxis = HIYAxis()
        let hiTitle_y = HITitle()
        hiTitle_y.text = "Point Value"
        yAxis.title = hiTitle_y

        let hiLabel_y = HILabels()
        hiLabel_y.align = "left"
        hiLabel_y.x = 3
        hiLabel_y.y = 16
        hiLabel_y.format = "{value:.,0f}"
        yAxis.labels = hiLabel_y

        options.yAxis = [yAxis]

        let hiToolTip = HITooltip()
        hiToolTip.shared = true

        options.tooltip = hiToolTip

        let plotOptions = HIPlotOptions()
        let hiSeries = HISeries()
        hiSeries.cursor = "pointer"
        let hiPoint = HIPoint()
        let hiEvent = HIEvents()
        let hiFunction = HIFunction()
        hiFunction.jsFunction = "function (e) { hs.htmlExpand(null, { pageOrigin: { x: e.pageX || e.clientX, y: e.pageY || e.clientY }, headingText: this.series.name, maincontentText: Highcharts.dateFormat('%A, %b %e, %Y', this.x) + ':<br/> ' + this.y + ' visits', width: 200 }); }"
        hiEvent.click = hiFunction
        hiPoint.events = hiEvent
        hiSeries.point = hiPoint
        let hiMarker = HIMarker()
        hiMarker.lineWidth = 1
        hiSeries.marker = hiMarker
        plotOptions.series = hiSeries
        options.plotOptions = plotOptions

        let hiLine = HILine()
        hiLine.name = "Point value"
        hiLine.lineWidth = 4
        let hiMarkerLine = HIMarker()
        hiMarkerLine.radius = 4
        hiLine.marker = hiMarkerLine
        hiLine.data = listValue

        let hiResponsive = HIResponsive()
        let hiRules = HIRules()
        let hiCondition = HICondition()
        hiCondition.maxWidth = self.uiViewLineChart_PlaceHolder.frame.size.width as NSNumber
        hiRules.condition = hiCondition
        hiRules.chartOptions = ["legend": ["verticalAlign": "bottom", "y":0, "floating":false]]
        hiResponsive.rules = [hiRules]
        options.responsive = hiResponsive

        options.series = [hiLine]

        self.chartView.options = options

    }
    
    @IBAction func btn_GoBack_OnClick(_ sender: Any) {
        super.viewWillDisappear(false)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
}
