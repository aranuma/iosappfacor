//
//  CameraVC.swift
//  Aranuma
//
//  Created by topcoder on 11/17/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class CameraVC: UIViewController {

    @IBOutlet weak var lblUpdatedTime: UILabel!
    @IBOutlet weak var imageCamera: UIImageView!
    @IBOutlet weak var lblLiveText: UILabel!
    
    var timerUpdateImage = Timer()
    
    var activityIndicator : NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblUpdatedTime.text = ""
        
        let xAxis = self.view.frame.size.width / 2
        let yAxis = self.view.frame.size.height / 2
        
        let frame = CGRect(x: xAxis - 25, y: yAxis - 25, width: 50, height: 50)
        activityIndicator = NVActivityIndicatorView(frame: frame)
        activityIndicator.type = .ballRotateChase
        activityIndicator.color = #colorLiteral(red: 0.5882352941, green: 0, blue: 0.1490196078, alpha: 1)
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        GetPointService.instance.pointlists.removeAll()
        updateImage()
        timerUpdateImage = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(updateImage), userInfo: nil, repeats: true)
    }
    
    @objc func updateImage() {
        DispatchQueue.global(qos: .background).async {
            GetPointService.instance.GetImageFromCamera(completion: {
                (success) in
                OperationQueue.main.addOperation({
                    if success {
                        if GetPointService.instance.pointlists.isEmpty == false {
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.removeFromSuperview()
                            self.DecodeBase64ToImage(Base64: GetPointService.instance.pointlists[0].pointValue)
                            self.lblUpdatedTime.text = "Last updated: " + GetPointService.instance.pointlists[0].pointTimeStamp
                            self.lblLiveText.text = "LiveCamera_1"
                            GetPointService.instance.pointlists.removeAll()
                        } else {
                            self.updateImage()
                        }
                    } else {
                        self.updateImage()
                    }
                })
            })
        }
    }
    
    func DecodeBase64ToImage(Base64 strBase64: String) {
        let dataDecoded: NSData = NSData(base64Encoded: strBase64, options: NSData.Base64DecodingOptions(rawValue: 0))!
        let decodedimage: UIImage = UIImage(data: dataDecoded as Data)!
        imageCamera.image = decodedimage
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(false)
        GetPointService.instance.pointlists.removeAll()
        self.timerUpdateImage.invalidate()
    }
}
