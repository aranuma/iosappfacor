//
//  ViewController.swift
//  Aranuma
//
//  Created by topcoder on 10/27/18.
//  Copyright © 2018 Aranuma. All rights reserved.
//

import UIKit

class SplashScreen: UIViewController {

    @IBOutlet weak var mainPapho: UIImageView!
    @IBOutlet weak var rightPapho: UIImageView!
    @IBOutlet weak var leftPapho: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Init()
        self.AnumationLoading()
    }
    
    func Init() {
        self.mainPapho.alpha = 0.0
    }
    
    func AnumationLoading() {
        if self.mainPapho.alpha == 0.0  {
            // show us view with fade in fade out animations
            UIImageView.animate(withDuration: 1, delay: 0.2, options: [.repeat, .autoreverse], animations: {
                self.mainPapho.alpha = 1.0
            })
        }
        
        self.rightPapho.transform = CGAffineTransform(translationX: 0, y: -200).concatenating(CGAffineTransform(scaleX: 0, y: 0))
        self.rightPapho.alpha = 0
        UIImageView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
            self.rightPapho.transform = .identity
            self.rightPapho.alpha = 1
        } ,completion: nil)
        
        self.leftPapho.transform = CGAffineTransform(translationX: 0, y: -200).concatenating(CGAffineTransform(scaleX: 0, y: 0))
        self.leftPapho.alpha = 0
        UIImageView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 0.6, initialSpringVelocity: 10, options: [.curveEaseOut], animations: {
            self.leftPapho.transform = .identity
            self.leftPapho.alpha = 1
        }, completion: nil)
    }
}
