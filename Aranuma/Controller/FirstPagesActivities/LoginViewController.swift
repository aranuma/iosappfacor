//
//  LoginViewController.swift
//  Aranuma
//
//  Created by topcoder on 10/29/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import UIKit
import TransitionButton

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textViewUseranme: CustomTextField_Login!
    @IBOutlet weak var textViewPassword: CustomTextField_Login!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        InitControls()
    }
    
    func InitControls() {
        
        let xAxis = self.view.frame.size.width / 2
        let yAxis = self.view.frame.size.height
        
        let button = TransitionButton(frame: CGRect(x: xAxis - 35, y: yAxis - 215, width: 70, height: 70))
        button.backgroundColor = #colorLiteral(red: 0.5882352941, green: 0, blue: 0.1490196078, alpha: 1)
        button.setTitle("Login", for: .normal)
        button.cornerRadius = 35
        button.spinnerColor = .white
        self.view.addSubview(button)
        button.addTarget(self, action: #selector(buttonLoginAction(_:)), for: .touchUpInside)
    }
    
    @IBAction func buttonLoginAction(_ button: TransitionButton) {
        button.startAnimation()
        
        let qualityOfServiceClass = DispatchQoS.QoSClass.background
        let backgroundQueue = DispatchQueue.global(qos: qualityOfServiceClass)
        if let username = self.textViewUseranme.text, let password = self.textViewPassword.text {
            backgroundQueue.async(execute: {
                if username != "" && password != "" {
                    AuthService.instance.UserLogin(username: username, password: password, completion: {
                        (success) in
                        if success && AuthService.instance.isLoggedIn {
                            DispatchQueue.main.async(execute: { () -> Void in
                                button.stopAnimation(animationStyle: .expand, completion: {
                                    
                                    //add the session in database
                                    UserActions.instance.SetSession(token: AuthService.instance.UserToken, _session: DeviceModel.instance.GetDeviceModle(), completion: {
                                        (response) in
                                    })
                                    
                                    //save user token in database
                                    let userAuthManagement = UsersAuth(context: PersistanceService.context)
                                    userAuthManagement.token = AuthService.instance.UserToken
                                    userAuthManagement.username = username
                                    PersistanceService.saveContext()
                                    //end
                                    
                                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let newViewController = storyBoard.instantiateViewController(withIdentifier: "MainPageViewController") as! MainTabBarVC
                                    self.present(newViewController, animated: true, completion: nil)
                                })
                            })
                        } else {
                            button.stopAnimation()
                            self.UnvalidLoginIn()
                        }
                    })
                } else {
                    button.stopAnimation()
                    self.UnvalidLoginIn()
                }
            })
        }
    }
    
    func UnvalidLoginIn() {
        let alert = UIAlertController(title: "Error", message: "username or password is wrong", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}
