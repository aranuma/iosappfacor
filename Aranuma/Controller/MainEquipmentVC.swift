//
//  MainEquipmentVC.swift
//  Aranuma
//
//  Created by topcoder on 11/3/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class MainEquipmentVC: UITableViewController {

    //---------------- HPGR Points
    @IBOutlet weak var imageview_led_motor_hpgr_1: UIImageView!
    @IBOutlet weak var imageview_led_motor_hpgr_2: UIImageView!
    
    @IBOutlet weak var lbl_point_value_hpgr_line1: UILabel!
    
    @IBOutlet weak var lbl_point_value_hpgr_line2: UILabel!
    @IBOutlet weak var lbl_point_timestamp_hpgr_line2: UILabel!
    
    @IBOutlet weak var lbl_point_value_hpgr_line3: UILabel!
    @IBOutlet weak var lbl_point_timestamp_hpgr_line3: UILabel!
    
    @IBOutlet weak var lbl_point_value_hpgr_line4: UILabel!
    
    @IBOutlet weak var lbl_point_value_hpgr_line5: UILabel!
    @IBOutlet weak var lbl_point_timestamp_hpgr_line5: UILabel!
    
    @IBOutlet weak var lbl_point_value_hpgr_line6: UILabel!
    @IBOutlet weak var lbl_point_timestamp_hpgr_line6: UILabel!
    
    @IBOutlet weak var lbl_point_value_hpgr_line7: UILabel!
    @IBOutlet weak var lbl_point_timestamp_hpgr_line7: UILabel!
    
    @IBOutlet weak var lbl_point_value_hpgr_line8: UILabel!
    @IBOutlet weak var lbl_point_timestamp_hpgr_line8: UILabel!
    
    //--------------- End Hpgr
    
    //--------------- BallMill Points
    @IBOutlet weak var imageview_led_motor_ballmill: UIImageView!

    @IBOutlet weak var lbl_point_value_ballmill_line1: UILabel!
    @IBOutlet weak var lbl_point_timestamp_ballmill_line1: UILabel!

    @IBOutlet weak var lbl_point_value_ballmill_line2: UILabel!
    @IBOutlet weak var lbl_point_timestamp_ballmill_line2: UILabel!
    
    @IBOutlet weak var lbl_point_value_ballmill_line3: UILabel!
    @IBOutlet weak var lbl_point_timestamp_ballmill_line3: UILabel!
    
    //---------------- End BallMill
    
    //---------------- Hydrocyclone Points
    @IBOutlet weak var lbl_point_value_hydrocyclone_line1: UILabel!
    @IBOutlet weak var lbl_point_timestamp_hydrocyclone_line1: UILabel!
    
    @IBOutlet weak var lbl_point_value_hydrocyclone_line2: UILabel!
    @IBOutlet weak var lbl_point_timestamp_hydrocyclone_line2: UILabel!
    //---------------- End Hydrocyclone
    
    //---------------- Thickener Points
    @IBOutlet weak var imageView_motor_thickener_led: UIImageView!
    
    @IBOutlet weak var lbl_point_value_thickener_line1: UILabel!
    
    @IBOutlet weak var lbl_point_value_thickener_line2: UILabel!
    @IBOutlet weak var lbl_point_timestamp_thickener_line2: UILabel!
    
    @IBOutlet weak var lbl_point_value_thickener_line3: UILabel!
    @IBOutlet weak var lbl_point_timestamp_thickener_line3: UILabel!
    
    @IBOutlet weak var lbl_point_value_thickener_line4: UILabel!
    @IBOutlet weak var lbl_point_timestamp_thickener_line4: UILabel!
    
    @IBOutlet weak var lbl_point_value_thickener_line5: UILabel!
    @IBOutlet weak var lbl_point_timestamp_thickener_line5: UILabel!
    
    @IBOutlet weak var lbl_point_value_thickener_line6: UILabel!
    @IBOutlet weak var lbl_point_timestamp_thickener_line6: UILabel!
    //---------------- End Thickener
    
    var timerUpdateTableView = Timer()
    var activityIndicator : NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        InitAllObjects()
    }
    
    func InitAllObjects() {
        MainEquipmentService.instance.pointList.removeAll()
        
        let xAxis = self.view.frame.size.width / 2
        let yAxis = self.view.frame.size.height / 2
        
        let frame = CGRect(x: xAxis - 25, y: yAxis - 25, width: 50, height: 50)
        
        activityIndicator = NVActivityIndicatorView(frame: frame)
        activityIndicator.type = .ballRotateChase
        activityIndicator.color = #colorLiteral(red: 0.5882352941, green: 0, blue: 0.1490196078, alpha: 1)
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        updateTable()
        
        timerUpdateTableView = Timer.scheduledTimer(timeInterval: 0.7, target: self,
                                                    selector: #selector(updateTable), userInfo: nil, repeats: true)
    }
    
    var counter: Int8 = 0
    @objc func updateTable() {
        DispatchQueue.global(qos: .background).async {
            MainEquipmentService.instance.GetPoints(completion: {
                (success) in
                OperationQueue.main.addOperation({
                    if success {
                        
                        if self.counter == 0 {
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.removeFromSuperview()
                            self.counter += 1
                        }
                        
                        do {
                            try self.PointsShow()
                        } catch {
                            debugPrint(error.localizedDescription)
                        }
                    } else {
                        self.updateTable()
                    }
                })
            })
        }
    }
    
    func PointsShow() throws {
        if MainEquipmentService.instance.pointList.isEmpty == false {
            var pointList = MainEquipmentService.instance.pointList
            
            let floatValue = (pointList[0].pointValue as NSString).floatValue
            let fixValue = (pointList[1].pointValue as NSString).floatValue
            let rollSpeed: Float = (floatValue + fixValue) / 2
            
            self.lbl_point_value_hpgr_line1.text = "Roll Speed: " + (NSString(format: "%.2f", rollSpeed) as String) as String + " rpm"
           
            self.lbl_point_value_hpgr_line2.text = pointList[2].pointName! + ": " + pointList[2].pointValue! + " KW"
            self.lbl_point_timestamp_hpgr_line2.text = pointList[2].pointTimeStamp!
            
            self.lbl_point_value_hpgr_line3.text = pointList[3].pointName! + ": " + pointList[3].pointValue! + " KW"
            self.lbl_point_timestamp_hpgr_line3.text = pointList[3].pointTimeStamp!
            
            let hpgr_val1 = (pointList[2].pointValue as NSString).floatValue
            let hpgr_val2 = (pointList[3].pointValue as NSString).floatValue
            let sum_hpgrs: Float = hpgr_val1 + hpgr_val2
            
            self.lbl_point_value_hpgr_line4.text = "Sum(Powers): " + (NSString(format: "%.2f", sum_hpgrs) as String) as String + " KW"
            
            self.lbl_point_value_hpgr_line5.text = pointList[4].pointName! + ": " + pointList[4].pointValue! + " mm"
            self.lbl_point_timestamp_hpgr_line5.text = pointList[4].pointTimeStamp!
            
            self.lbl_point_value_hpgr_line6.text = pointList[5].pointName! + ": " + pointList[5].pointValue! + " mm"
            self.lbl_point_timestamp_hpgr_line6.text = pointList[5].pointTimeStamp!
            
            self.lbl_point_value_hpgr_line7.text = pointList[6].pointName! + ": " + pointList[6].pointValue! + " bar"
            self.lbl_point_timestamp_hpgr_line7.text = pointList[6].pointTimeStamp!
            
            self.lbl_point_value_hpgr_line8.text = pointList[7].pointName! + ": " + pointList[7].pointValue! + " bar"
            self.lbl_point_timestamp_hpgr_line8.text = pointList[7].pointTimeStamp!
            //........................
            
            self.lbl_point_value_ballmill_line1.text = pointList[8].pointName! + ": " + pointList[8].pointValue! + " MW/H"
            self.lbl_point_timestamp_ballmill_line1.text = pointList[8].pointTimeStamp!
            
            self.lbl_point_value_ballmill_line2.text = pointList[9].pointName! + ": " + pointList[9].pointValue! + " mm/s"
            self.lbl_point_timestamp_ballmill_line2.text = pointList[9].pointTimeStamp!
            
            self.lbl_point_value_ballmill_line3.text = pointList[10].pointName! + ": " + pointList[10].pointValue! + " m3/h"
            self.lbl_point_timestamp_ballmill_line3.text = pointList[10].pointTimeStamp!
            
            //.
            
            self.lbl_point_value_hydrocyclone_line1.text = pointList[11].pointName! + ": " + pointList[11].pointValue! + " bar"
            self.lbl_point_timestamp_hydrocyclone_line1.text = pointList[11].pointTimeStamp!
            
            self.lbl_point_value_hydrocyclone_line2.text = pointList[12].pointName! + ": " + pointList[12].pointValue! + " t/m3"
            self.lbl_point_timestamp_hydrocyclone_line2.text = pointList[12].pointTimeStamp!
            
            //.......................................
            
            self.lbl_point_value_thickener_line2.text = pointList[13].pointName! + ": " + pointList[13].pointValue! + " cm"
            self.lbl_point_timestamp_thickener_line2.text = pointList[13].pointTimeStamp!
            
            self.lbl_point_value_thickener_line3.text = pointList[14].pointName! + ": " + pointList[14].pointValue! + " cm"
            self.lbl_point_timestamp_thickener_line3.text = pointList[14].pointTimeStamp!
            
            self.lbl_point_value_thickener_line4.text = pointList[15].pointName! + ": " + pointList[15].pointValue! + " %"
            self.lbl_point_timestamp_thickener_line4.text = pointList[15].pointTimeStamp!
            
            self.lbl_point_value_thickener_line5.text = pointList[16].pointName! + ": " + pointList[16].pointValue! + " m-bar"
            self.lbl_point_timestamp_thickener_line5.text = pointList[16].pointTimeStamp!
            
            self.lbl_point_value_thickener_line6.text = pointList[17].pointName! + ": " + pointList[17].pointValue! + " %"
            self.lbl_point_timestamp_thickener_line6.text = pointList[17].pointTimeStamp!
            
            if pointList[18].pointValue == "1" {
                self.imageview_led_motor_hpgr_1.image = UIImage(named: "led_green")
            } else {
                self.imageview_led_motor_hpgr_1.image = UIImage(named: "led_red")
            }
            
            if pointList[19].pointValue == "1" {
                self.imageview_led_motor_hpgr_2.image = UIImage(named: "led_green")
            } else {
                self.imageview_led_motor_hpgr_2.image = UIImage(named: "led_red")
            }
            
            if pointList[20].pointValue == "1" {
                self.imageview_led_motor_ballmill.image = UIImage(named: "led_green")
            } else {
                self.imageview_led_motor_ballmill.image = UIImage(named: "led_red")
            }

            if pointList[21].pointValue == "1" {
                self.imageView_motor_thickener_led.image = UIImage(named: "led_green")
            } else {
                self.imageView_motor_thickener_led.image = UIImage(named: "led_red")
            }

            if pointList[22].pointValue == "1" {
                self.lbl_point_value_thickener_line1.text = pointList[22].pointName! + ": On"
            } else {
                self.lbl_point_value_thickener_line1.text = pointList[22].pointName! + ": Off"
            }
            
            MainEquipmentService.instance.pointList.removeAll()
        } else {
            updateTable()
        }
        
    }
    
    @IBAction func unwindFromMainEquipmentVC(_ sender: UIButton) {
        timerUpdateTableView.invalidate()
        MainEquipmentService.instance.pointList.removeAll()
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(false)
        timerUpdateTableView.invalidate()
        MainEquipmentService.instance.pointList.removeAll()
        self.dismiss(animated: true, completion: nil)
    }
    
}
