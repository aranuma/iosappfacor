//
//  UtilityAccessoryVC.swift
//  Aranuma
//
//  Created by topcoder on 11/3/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class UtilityAccessoryVC: UITableViewController {
    
    @IBOutlet weak var lbl_Level_line1_value: UILabel!
    @IBOutlet weak var lbl_Level_line2_value: UILabel!
    @IBOutlet weak var lbl_Level_line2_timestamp: UILabel!
    @IBOutlet weak var lbl_Level_line3_value: UILabel!
    @IBOutlet weak var lbl_Level_line3_timestamp: UILabel!
    
    var timerUpdateTableView = Timer()
    var activityIndicator: NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        InitAllObjects()
    }
    
    func InitAllObjects() {
        UtilityAccessoryService.instance.pointsList.removeAll()
        
        let xAxis = self.view.frame.size.width / 2
        let yAxis = self.view.frame.size.height / 2
        
        let frame = CGRect(x: xAxis - 25, y: yAxis - 25, width: 50, height: 50)
        activityIndicator = NVActivityIndicatorView(frame: frame)
        activityIndicator.type = .ballRotateChase
        activityIndicator.color = #colorLiteral(red: 0.5882352941, green: 0, blue: 0.1490196078, alpha: 1)
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        updateTable()
        timerUpdateTableView = Timer.scheduledTimer(timeInterval: 0.7, target: self,
                                                    selector: #selector(updateTable), userInfo: nil, repeats: true)
    }
    
    var counter: Int8 = 0
    @objc func updateTable() {
        DispatchQueue.global(qos: .background).async {
            UtilityAccessoryService.instance.GetPoints(completion: {
                (success) in
                OperationQueue.main.addOperation ({
                    if success {
                        if self.counter == 0 {
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.removeFromSuperview()
                            self.counter += 1
                        }
                        
                        do {
                            try self.PointShow()
                        } catch {
                            debugPrint(error.localizedDescription)
                        }
                    } else {
                        self.updateTable()
                    }
                })
            })
        }
    }
    
    func PointShow() throws {
        if UtilityAccessoryService.instance.pointsList.isEmpty == false {
            var pointList = UtilityAccessoryService.instance.pointsList
            
            let makeup1 = (pointList[0].pointValue as NSString).floatValue
            let makeup2 = (pointList[1].pointValue as NSString).floatValue
            let MakeUpLevel: Float = (makeup1 + makeup2) / 2
            
            self.lbl_Level_line1_value.text = "Makeup level: " + (NSString(format: "%.2f", MakeUpLevel) as String) as String + " %"
            
            self.lbl_Level_line2_value.text = pointList[2].pointName! + ": " + pointList[2].pointValue! + " %"
            self.lbl_Level_line2_timestamp.text = pointList[2].pointTimeStamp!
            
            self.lbl_Level_line3_value.text = pointList[3].pointName! + ": " + pointList[3].pointValue! + " %"
            self.lbl_Level_line3_timestamp.text = pointList[3].pointTimeStamp!
            
            UtilityAccessoryService.instance.pointsList.removeAll()
        } else {
            updateTable()
        }
    }
    
    @IBAction func unwindUtilityAccessoryVC(_ sender: UIButton) {
        timerUpdateTableView.invalidate()
        UtilityAccessoryService.instance.pointsList.removeAll()
        self.dismiss(animated: true, completion: nil)
    }
}
