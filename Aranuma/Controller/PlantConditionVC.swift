//
//  PlantConditionVC.swift
//  Aranuma
//
//  Created by topcoder on 11/3/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class PlantConditionVC: UITableViewController {

    @IBOutlet weak var lblCurrent_Input_value: UILabel!
    @IBOutlet weak var lblCurrent_Input_timestamp: UILabel!
   
    @IBOutlet weak var lblTotal_Input_value: UILabel!
    @IBOutlet weak var lblTotal_Input_timestamp: UILabel!
    
    @IBOutlet weak var lblShift_Input_value: UILabel!
    @IBOutlet weak var lblShift_Input_timestamp: UILabel!
    
    @IBOutlet weak var lblCurrent_Output_value: UILabel!
    @IBOutlet weak var lblCurrent_Output_timestamp: UILabel!
    
    @IBOutlet weak var lblTotal_Output_value: UILabel!
    @IBOutlet weak var lblTotal_Output_timestamp: UILabel!
    
    @IBOutlet weak var lblShift_Output_value: UILabel!
    @IBOutlet weak var lblShift_Output_timestamp: UILabel!
    
    @IBOutlet weak var lblFreshWater_Line1_value: UILabel!
    @IBOutlet weak var lblFreshWater_Line1_timestamp: UILabel!
    
    @IBOutlet weak var lblGeneral_Line1_Value: UILabel!
    
    @IBOutlet weak var lblGeneral_Line2_Value: UILabel!
    @IBOutlet weak var lblGeneral_Line2_Timestamp: UILabel!
    
    @IBOutlet weak var plc_pulse_ballmill_image: UIImageView!
    @IBOutlet weak var plc_pulse_hpgr_image: UIImageView!
    
    
    var timerUpdateTableView = Timer()
    var activityIndicator : NVActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        InitAllObjects()
    }
    
    func InitAllObjects() {
        PlantConditionService.instance.pointsList.removeAll()
        
        let xAxis = self.view.frame.size.width / 2
        let yAxis = self.view.frame.size.height / 2
        
        let frame = CGRect(x: xAxis - 25, y: yAxis - 25, width: 50, height: 50)
        activityIndicator = NVActivityIndicatorView(frame: frame)
        activityIndicator.type = .ballRotateChase
        activityIndicator.color = #colorLiteral(red: 0.5882352941, green: 0, blue: 0.1490196078, alpha: 1)
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        updateTable()

        timerUpdateTableView = Timer.scheduledTimer(timeInterval: 0.7, target: self,
                 selector: #selector(updateTable), userInfo: nil, repeats: true)
    }
    
    var counter: Int8 = 0
    @objc func updateTable() {
        DispatchQueue.global(qos: .background).async {
            PlantConditionService.instance.GetPoints(completion: {
                (success) in
                OperationQueue.main.addOperation({
                    if success {
                        if self.counter == 0 {
                            self.activityIndicator.stopAnimating()
                            self.activityIndicator.removeFromSuperview()
                            self.counter += 1
                        }
                        self.PointsShow()
                    } else {
                        self.updateTable()
                    }
                })
            })
        }
    }
    
    func PointsShow() {
        if PlantConditionService.instance.pointsList.isEmpty == false {
            var pointList = PlantConditionService.instance.pointsList
            
            self.lblCurrent_Input_value.text = pointList[0].pointName! + ": " + pointList[0].pointValue! + " t/h"
            self.lblCurrent_Input_timestamp.text = pointList[0].pointTimeStamp!
            
            self.lblTotal_Input_value.text = pointList[1].pointName! + ": " + pointList[1].pointValue! + " t"
            self.lblTotal_Input_timestamp.text = pointList[1].pointTimeStamp!
            
            self.lblShift_Input_value.text = pointList[2].pointName! + ": " + pointList[2].pointValue! + " t"
            self.lblShift_Input_timestamp.text = pointList[2].pointTimeStamp!
            
            self.lblCurrent_Output_value.text = pointList[3].pointName! + ": " + pointList[3].pointValue! + " t/h"
            self.lblCurrent_Output_timestamp.text = pointList[3].pointTimeStamp!
            
            self.lblTotal_Output_value.text = pointList[4].pointName! + ": " + pointList[4].pointValue! + " t"
            self.lblTotal_Output_timestamp.text = pointList[4].pointTimeStamp!
            
            self.lblShift_Output_value.text = pointList[5].pointName! + ": " + pointList[5].pointValue! + " t"
            self.lblShift_Output_timestamp.text = pointList[5].pointTimeStamp!
            
            self.lblFreshWater_Line1_value.text = pointList[6].pointName! + ": " + pointList[6].pointValue! + " m3/h"
            
            self.lblFreshWater_Line1_timestamp.text = pointList[6].pointTimeStamp!
            
            let concentrateOut = (pointList[4].pointValue as NSString).floatValue
            let concentrateInput = (pointList[1].pointValue as NSString).floatValue
            let output_input: Float = concentrateOut / (concentrateInput) * 100 * 0.92
            self.lblGeneral_Line1_Value.text = "Mass Shift Recovery: " + (NSString(format: "%.2f", output_input) as String) as String + " %"
            
            self.lblGeneral_Line2_Value.text = pointList[7].pointName! + ": " + pointList[7].pointValue! + " h"
            self.lblGeneral_Line2_Timestamp.text = pointList[7].pointTimeStamp!
            
            if pointList[8].pointValue == "1" {
                self.plc_pulse_ballmill_image.image = UIImage(named: "led_green")
            } else {
                self.plc_pulse_ballmill_image.image = UIImage(named: "led_red")
            }
            
            if pointList[9].pointValue == "1" {
                self.plc_pulse_hpgr_image.image = UIImage(named: "led_green")
            } else {
                self.plc_pulse_hpgr_image.image = UIImage(named: "led_red")
            }
            
            PlantConditionService.instance.pointsList.removeAll()
        } else {
            updateTable()
        }
    }
    
    @IBAction func unwindFromPlantConditionVC(_ sender: UIButton) {
        timerUpdateTableView.invalidate()
        PlantConditionService.instance.pointsList.removeAll()
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(false)
        timerUpdateTableView.invalidate()
        PlantConditionService.instance.pointsList.removeAll()
        self.dismiss(animated: true, completion: nil)
    }
    
}
