//
//  MainViewController.swift
//  Aranuma
//
//  Created by topcoder on 10/29/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserActions.instance.GetAppVersion(completion: {
            (success) in
            if success {
                if UserActions.instance.appVersion != "1.0.2.2" {
                    let alert = UIAlertController(title: "Info", message: "New version of application is available as: " + UserActions.instance.appVersion, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        })
    }
    
    @IBAction func onClick_btnPlantCondition(_ sender: Any) {
        performSegue(withIdentifier: "idGoToPlantConditionActivity", sender: self)
    }
    
    @IBAction func onClick_btnMainEquipment(_ sender: Any) {
        performSegue(withIdentifier: "idGoToMainEquipmentActivity", sender: self)
    }
    
    @IBAction func onClick_btnUtilityAccessory(_ sender: Any) {
        performSegue(withIdentifier: "idGoToUtilityAccessoryVC", sender: self)
    }
}
