//
//  Calculate.swift
//  Aranuma
//
//  Created by topcoder on 11/28/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import Foundation

class Calculate {
    static let instance = Calculate()
    
    func GetMaximum(arrayList: [Float32]) -> [Float32] {
        let max = arrayList.max()
        let index = arrayList.index(of: max ?? 0)
        return [max ?? 0, Float32(index ?? 0)]
    }
    
    func GetMinimum(arrayList: [Float32]) -> [Float32] {
        let min = arrayList.min()
        let index = arrayList.index(of: min ?? 0)
        return [min ?? 0, Float32(index ?? 0)]
    }
    
    func GetAverage(arrayList: [Float32]) -> Float32 {
        var sum: Float32 = 0
        for item in arrayList {
            sum += item
        }
        return sum / Float(arrayList.count)
    }
    
    func GetMedian(arrayList: [Float32]) -> Float32 {
        let newArray = arrayList.sorted()
        if arrayList.count % 2 == 0 {
            return (newArray[newArray.count / 2] + newArray[newArray.count / 2 - 1]) / 2
        } else {
            return (newArray[newArray.count / 2])
        }
    }
    
    func GetVariance(arrayList: [Float32]) -> Float32 {
        var variance: Float32 = 0
        let avarage: Float = GetAverage(arrayList: arrayList)
        
        for item in arrayList {
            variance += pow(item - avarage, 2)
        }
        return variance
    }
}
