//
//  Constants.swift
//  Aranuma
//
//  Created by topcoder on 11/4/18.
//  Copyright © 2018 aranuma. All rights reserved.
//

import Foundation

typealias CompletionHandler = (_ Success: Bool) -> ()

//urls
let BASE_URL: String = "https://araserver.aranuma.com/"
let URL_LOGIN: String = "\(BASE_URL)api/Users/UsersAuth"
let URL_GET_SelectedPoint: String = "\(BASE_URL)api/UserDatahubPoint/GetSelectedPoint"
let URL_GET_ChartService: String = "\(BASE_URL)api/Charts/MakeChart"
let URL_GET_GetAllListedPoints: String = "\(BASE_URL)api/UserDatahubPoint/GetAllPoints"
let URL_IS_TOKENVALID: String = "\(BASE_URL)api/Users/IsTokenValidMob"
let URL_GET_SESSIONS: String = "\(BASE_URL)api/Users/GetAllSessionForUser"
let URL_SET_SESSION: String = "\(BASE_URL)api/Users/SetSessions"
let URL_SET_UserLog: String = "\(BASE_URL)api/Users/SetUserLog"
let URL_GET_GetAllPointsOfSelectedDevice_Charts: String = "\(BASE_URL)api/Charts/GetPointsOfCategory"
let URL_GET_GetAllDevicePoints_Charts: String = "\(BASE_URL)api/Charts/GetDevicesCategories"
let URL_GET_GetAllSessionForUsers: String = "\(BASE_URL)api/Users/GetAllSessionForUser"
let URL_POST_ChangePassword: String = "\(BASE_URL)api/Users/ChangePassword"
let URL_GET_AllUserLogs: String = "\(BASE_URL)api/Users/GetAllUsersLog"
let URL_GET_APPVeriosn: String = "\(BASE_URL)api/Users/GetIOSAppVersion"

let URL_GET_ALLPointForFilter: String = "\(BASE_URL)api/Points/GetAllPointsForFilter"
let URL_GET_ALLSelectedPoints: String = "\(BASE_URL)api/Points/GetAllFilteredPoint"

//end of urls

//variable constants
let LOGGED_IN_KEY = "loggedIn"
let USER_ID = "USER_ID"
let USER_TOKEN = "USER_TOKEN"
//end variables

let HEADER = [
    "Content-Type": "application/json; charset=utf-8"
]
// code test below



